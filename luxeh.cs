using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

internal sealed class luxeh
{
    private class ChearTriggerVariables
    {
        internal int triggerDelay;

        internal float distance;

        internal uint forceAttackForWeapon;

        internal float[] targetPosition;

        internal bool lastShootSet;

        internal int crosshairdId;

        internal bool targetOutOfDistance;

        internal int triggerPause;

        internal uint crosshairTargetPtr;

        internal int crosshairTargetTeam;

        internal long lastShootTime;

        internal bool TriggerIsrunning;

        internal bool ShootDelayActive;

        internal int forceAttackFlags;

        internal float[] localplayerPosition;

        internal byte[] positionBuffer;

        public ChearTriggerVariables()
        {
            triggerDelay = 0;
            distance = 0f;
            forceAttackForWeapon = 0u;
            targetPosition = new float[3];
            lastShootSet = false;
            crosshairdId = 0;
            targetOutOfDistance = false;
            triggerPause = 0;
            crosshairTargetPtr = 0u;
            crosshairTargetTeam = 0;
            lastShootTime = 0L;
            TriggerIsrunning = false;
            ShootDelayActive = false;
            forceAttackFlags = 0;
            localplayerPosition = new float[3];
            positionBuffer = new byte[12];
        }
    }

    private class CheatConfig
    {
        internal uint[] TriggerKeybind;

        internal int[] TriggerDelay;

        internal int[] TriggerPause;

        internal long[] TriggerAfterShot;

        internal uint PanicKey;

        internal bool RadarEnabled;

        internal bool FriendlyFire;

        internal int MasterBreak;

        internal float NoFlashAmount;

        internal bool PremiumUser_Visuals;

        internal uint GlowESPKeybind;

        internal uint BhopKey;

        internal float[] AimbotFov;

        internal uint[] AimbotKey;

        internal float[] AimbotSmooth;

        internal bool[] AimbotRotationAssist;

        internal int[,] AimbotBone;

        internal bool[] AimbotThroughWalls;

        internal int[] AimbotStick;

        internal int[] AimbotBoneSeletion;

        internal long[] AimDelay;

        internal long[] AimTime;

        internal bool SoundESPEnabled;

        internal uint SoundESPPitch;

        internal float SoundESPMaxRange;

        internal int int_6;

        internal bool FaceitMode;

        internal float[] VerticalRecoil;

        internal float[] HorizontalRecoil;

        public CheatConfig()
        {
            TriggerKeybind = new uint[37];
            TriggerDelay = new int[37];
            TriggerPause = new int[37];
            TriggerAfterShot = new long[37];
            PanicKey = 0u;
            FriendlyFire = false;
            MasterBreak = 1;
            PremiumUser_Visuals = true;
            GlowESPKeybind = 0u;
            BhopKey = 0u;
            AimbotFov = new float[37];
            AimbotKey = new uint[37];
            AimbotSmooth = new float[37];
            AimbotRotationAssist = new bool[37];
            AimbotBone = new int[37, 4];
            AimbotThroughWalls = new bool[37];
            AimbotStick = new int[37];
            AimbotBoneSeletion = new int[37];
            AimDelay = new long[37];
            AimTime = new long[37];
            SoundESPEnabled = false;
            SoundESPPitch = 0u;
            SoundESPMaxRange = 0f;
            int_6 = 0;
            FaceitMode = false;
            VerticalRecoil = new float[37];
            HorizontalRecoil = new float[37];
        }
    }

    private struct MEMORY_BASIC_INFORMATION
    {
        public IntPtr BaseAddress;

        public IntPtr AllocationBase;

        public uint AllocationProtect;

        public IntPtr RegionSize;

        public uint State;

        public uint Protect;

        public uint Type;
    }

    public struct INPUT
    {
        public int type;

        public MOUSEINPUT mi;
    }

    private class ChearBunyHopVariables
    {
        internal bool bool_0;

        internal bool BunnyhopActive;

        internal int flags;

        internal int forceJump;

        public ChearBunyHopVariables()
        {
            bool_0 = false;
            BunnyhopActive = false;
            flags = 0;
            forceJump = 0;
        }
    }

    private struct MSLLHOOKSTRUCT
    {
        public IntPtr pt;

        public uint mouseData;

        public uint flags;

        public uint time;

        public UIntPtr dwExtraInfo;
    }

    private delegate uint ZwWriteVirtualMemory_t(IntPtr intptr_0, IntPtr intptr_1, byte[] byte_0, int int_0, ref int int_1);

    private delegate int HookCallbackMouse_t(int nCode, int wParam, ref MSLLHOOKSTRUCT lParam);

    private delegate IntPtr SetWindowsHookExA_keyboard_t(int int_0, HookCallbackKeyboard_t delegate26_0, IntPtr intptr_0, uint uint_0);

    private class CheatOffsets
    {
        internal uint dwZoomSensitivityRatioPtr;

        internal uint dwForceAttack2;

        internal uint m_hActiveWeapon;

        internal uint m_vecVelocity;

        internal uint m_vecOrigin;

        internal uint dwMouseEnable;

        internal uint m_lifeState;

        internal uint dwSensitivityPtr;

        internal uint m_pitchClassPtr;

        internal uint dwForceAttack;

        internal uint m_bInReload;

        internal uint m_iTeamNum;

        internal uint m_bDormant;

        internal uint dwYawPtr;

        internal uint m_iCrosshairId;

        internal uint m_iShotsFired;

        internal uint dwLocalPosition;

        internal uint m_bIsScoped;

        internal uint dwClientState;

        internal uint dwPlayerResource;

        internal uint m_dwBoneMatrix;

        internal uint dwForceJump;

        internal uint m_iClip1;

        internal uint dwGlowObjectManager;

        internal uint dwEntityList;

        internal uint m_bSpottedByMask;

        internal uint m_flFlashMaxAlpha;

        internal uint dwLocalPlayer;

        internal uint m_fFlags;

        internal uint interval_per_tick;

        internal uint m_aimPunchAngle;

        internal uint m_iGlowIndex;

        internal uint dwClientState_ViewAngles;

        internal uint dwClientState_State;

        internal uint dwGlobalVars;

        internal uint m_bSpotted;

        internal uint unsuedOffset;

        internal uint m_iItemDefinitionIndex;

        public CheatOffsets()
        {
            dwZoomSensitivityRatioPtr = 0u;
            dwForceAttack2 = 0u;
            m_hActiveWeapon = 12024u;
            m_vecVelocity = 276u;
            m_vecOrigin = 312u;
            dwMouseEnable = 0u;
            m_lifeState = 607u;
            dwSensitivityPtr = 0u;
            m_pitchClassPtr = 0u;
            dwForceAttack = 0u;
            m_bInReload = 12965u;
            m_iTeamNum = 244u;
            m_bDormant = 237u;
            dwYawPtr = 0u;
            m_iCrosshairId = 46052u;
            m_iShotsFired = 41872u;
            dwLocalPosition = 0u;
            m_bIsScoped = 14632u;
            dwClientState = 0u;
            dwPlayerResource = 0u;
            m_dwBoneMatrix = 9896u;
            dwForceJump = 0u;
            m_iClip1 = 12900u;
            dwGlowObjectManager = 0u;
            dwEntityList = 0u;
            m_bSpottedByMask = 2432u;
            m_flFlashMaxAlpha = 42012u;
            dwLocalPlayer = 0u;
            m_fFlags = 260u;
            interval_per_tick = 32u;
            m_aimPunchAngle = 12332u;
            m_iGlowIndex = 42040u;
            dwClientState_ViewAngles = 0u;
            dwClientState_State = 264u;
            dwGlobalVars = 0u;
            m_bSpotted = 2365u;
            unsuedOffset = 0u;
            m_iItemDefinitionIndex = 12202u;
        }
    }

    private struct Struct3
    {
        public uint uint_0;

        public byte[] byte_0;
    }

    private delegate bool GlobalUnlock_t(IntPtr intptr_0);

    public struct MOUSEINPUT
    {
        public int dx;

        public int dy;

        public uint mouseData;

        public uint dwFlags;

        public uint time;

        public UIntPtr dwExtraInfo;
    }

    private delegate bool Module32Next_t(IntPtr intptr_0, ref MODULEENTRY32 struct10_0);

    private class ChearAimbotVariables
    {
        internal float aimbotFov;

        internal long aimStartTime;

        internal uint targetBase;

        internal int selectedBone;

        internal float[] lastAimPunchValues;

        internal int lastboneIndex;

        internal float[] lastAimPunch;

        internal bool unsedValue2;

        internal uint lastTargetBase;

        internal float aimbotSmooth;

        internal bool aimbotStick;

        internal bool lastAimPunchIsZero;

        internal bool stickTarget;

        internal float fovStickValue;

        internal bool isTriggerKeyPressed;

        internal long unusedValue;

        internal long aimDelayValue;

        internal bool aimThroughWalls;

        internal bool aimbotKeyPressed;

        internal bool IsAimDelaySet;

        internal bool bool_8;

        internal long aimDelayValueSecond;

        internal bool aimbotTargetfound;

        internal long long_4;

        internal float[] viewAngles;

        internal bool AimbotIsrunning;

        public ChearAimbotVariables()
        {
            aimbotFov = cheatConfig.AimbotFov[iuxehHelper.normalWeaponsConfigIndex];
            aimStartTime = 0L;
            targetBase = 0u;
            selectedBone = GetSelectedBone();
            lastAimPunchValues = new float[4];
            lastboneIndex = 0;
            lastAimPunch = new float[2];
            unsedValue2 = false;
            lastTargetBase = 0u;
            aimbotSmooth = cheatConfig.AimbotSmooth[iuxehHelper.normalWeaponsConfigIndex];
            aimbotStick = (cheatConfig.AimbotStick[iuxehHelper.normalWeaponsConfigIndex] == 1);
            lastAimPunchIsZero = false;
            stickTarget = false;
            fovStickValue = 15f;
            isTriggerKeyPressed = false;
            unusedValue = 0L;
            aimDelayValue = 0L;
            aimThroughWalls = cheatConfig.AimbotThroughWalls[iuxehHelper.normalWeaponsConfigIndex];
            aimbotKeyPressed = false;
            IsAimDelaySet = true;
            bool_8 = false;
            aimDelayValueSecond = 0L;
            aimbotTargetfound = false;
            long_4 = 0L;
            viewAngles = new float[3];
            AimbotIsrunning = false;
        }
    }

    private struct tagMSG
    {
        public IntPtr hwnd;

        public uint message;

        public IntPtr wParam;

        public IntPtr lParam;

        public uint time;

        public IntPtr pt;
    }

    private delegate int CallNextHookEx_keyboard_t(IntPtr intptr_0, int int_0, int int_1, KBDLLHOOKSTRUCT struct14_0);

    private delegate int UnhookWindowsHookEx_t(IntPtr intptr_0);

    private struct PSTARTUPINFOA
    {
        public uint cb;

        public string lpReserved;

        public string lpDesktop;

        public string lpTitle;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 36)]
        public byte[] cbReserved2;

        public byte lpReserved2;

        public IntPtr hStdInput;

        public IntPtr hStdOutput;

        public IntPtr hStdError;
    }

    private delegate bool VirtualFree_t(IntPtr intptr_0, uint uint_0, uint uint_1);

    private struct BoneInfo
    {
        public int boneID;

        public float X;

        public float Y;

        public float Z;
    }

    private struct EntityInfo
    {
        public uint EntityBase;

        public int Index;

        public short TeamNum;

        public bool IsDormant;

        public byte LifeState;

        public int int_1;

        public bool IsAlly;

        public float[] vecOrigin;

        public float[] vecVelocity;
    }

    private delegate IntPtr GlobalLock_t(IntPtr intptr_0);

    private delegate bool CreateProcessAsUserA_t(IntPtr intptr_0, string string_0, string string_1, IntPtr intptr_1, IntPtr intptr_2, bool bool_0, uint uint_0, IntPtr intptr_3, string string_2, ref PSTARTUPINFOA struct6_0, ref PROCESS_INFORMATION struct13_0);

    private struct Struct9
    {
        public IntPtr intptr_0;

        public IntPtr intptr_1;
    }

    private delegate uint K32GetMappedFileNameA_t(IntPtr intptr_0, IntPtr intptr_1, StringBuilder stringBuilder_0, uint uint_0);

    private delegate uint SendInput_t(uint uint_0, INPUT[] struct1_0, int int_0);

    private delegate bool Beep_t(uint uint_0, uint uint_1);

    private delegate bool PostThreadMessageA_t(uint uint_0, uint uint_1, int int_0, IntPtr intptr_0);

    private struct MODULEENTRY32
    {
        public uint dwSize;

        public uint th32ModuleID;

        public uint th32ProcessID;

        public uint GlblcntUsage;

        public uint ProccntUsage;

        public IntPtr modBaseAddr;

        public uint modBaseSize;

        public IntPtr hModule;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string szModule;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string szExePath;
    }

    private delegate uint VirtualQueryEx_t(IntPtr intptr_0, IntPtr intptr_1, ref MEMORY_BASIC_INFORMATION struct0_0, uint uint_0);

    private delegate bool CloseClipboard_t();

    private struct OBJECT_ATTRIBUTES
    {
        public int Length;

        public IntPtr RootDirectory;

        public IntPtr ObjectName;

        public uint Attributes;

        public IntPtr SecurityDescriptor;

        public IntPtr SecurityQualityOfService;
    }

    private delegate uint GetWindowThreadProcessId_t(IntPtr intptr_0, ref IntPtr intptr_1);

    private struct CLIENT_ID
    {
        public IntPtr UniqueProcess;

        public IntPtr UniqueThread;
    }

    private delegate void mouse_event_t(uint uint_0, int int_0, int int_1, uint uint_1, UIntPtr uintptr_0);

    private struct PROCESS_INFORMATION
    {
        public IntPtr hProcess;

        public IntPtr hThread;

        public uint dwProcessId;

        public uint dwThreadId;
    }

    private class CheatVariables
    {
        internal IntPtr csgoHandle;

        internal IntPtr keyboardHookHandle;

        internal IntPtr mouseHookHandle;

        internal uint clientBaseAddress;

        internal uint engineBaseAddress;

        internal uint clientState;

        internal uint csgoPID;

        internal float float_0;

        internal Stopwatch cheatStartStopWatch;

        internal long startTime;

        internal bool isGlowActive;

        internal float Sensitivity;

        internal float pitchClass;

        internal float YawPtr;

        internal float floatZoomSensitivity_4;

        internal bool IsScoped;

        internal int activeWeaponZoomLevel;

        internal bool cheatIsRunning;

        internal int normalWeaponsConfigIndex;

        internal int specialWeaponConfigIndex;

        internal bool panicButtonActive;

        internal bool modulesLoaded;

        internal int HighestPlayerIndex;

        internal uint mouse_keyboardHookThreadID;

        internal uint lastTargetBase;

        internal bool[] keyState;

        internal bool is5EClient;

        public CheatVariables()
        {
            csgoHandle = default;
            keyboardHookHandle = default;
            mouseHookHandle = default;
            clientBaseAddress = 0u;
            engineBaseAddress = 0u;
            clientState = 0u;
            csgoPID = 0u;
            cheatStartStopWatch = new Stopwatch();
            startTime = 0L;
            isGlowActive = true;
            Sensitivity = 0f;
            pitchClass = 0f;
            YawPtr = 0f;
            floatZoomSensitivity_4 = 0f;
            IsScoped = false;
            cheatIsRunning = false;
            normalWeaponsConfigIndex = 0;
            specialWeaponConfigIndex = 0;
            panicButtonActive = false;
            modulesLoaded = false;
            keyState = new bool[256];
            is5EClient = false;
        }
    }

    private struct KBDLLHOOKSTRUCT
    {
        public uint vkCode;

        public uint scanCode;

        public uint flags;

        public uint time;

        public UIntPtr dwExtraInfo;
    }

    private delegate bool EmptyClipboard_t();

    private delegate uint NtOpenProcess_t(ref IntPtr intptr_0, uint uint_0, ref OBJECT_ATTRIBUTES struct11_0, ref CLIENT_ID struct12_0);

    private delegate bool GetMessageA_t(out tagMSG struct5_0, IntPtr intptr_0, uint uint_0, uint uint_1);

    private delegate IntPtr SetWindowsHookExA_mouse_t(int int_0, HookCallbackMouse_t delegate2_0, IntPtr intptr_0, uint uint_0);

    private delegate bool VirtualProtect_t(IntPtr intptr_0, uint uint_0, uint uint_1, ref uint uint_2);

    private delegate bool Module32First_t(IntPtr intptr_0, ref MODULEENTRY32 struct10_0);

    private delegate IntPtr LoadLibraryA_t(string string_0);

    private delegate int HookCallbackKeyboard_t(int nCode, int wParam, KBDLLHOOKSTRUCT kbdStruct);

    private delegate bool OpenClipboard_t(IntPtr intptr_0);

    private delegate bool IsClipboardFormatAvailable_t(uint uint_0);

    private delegate IntPtr GetForegroundWindow_t();

    private delegate IntPtr TranslateMessage_t([In] ref tagMSG struct5_0);

    private delegate void Sleep_t(uint uint_0);

    private delegate IntPtr CreateToolhelp32Snapshot_t(uint uint_0, uint uint_1);

    private delegate IntPtr GetClipboardData_t(uint uint_0);

    private delegate uint GetCurrentThreadId_t();

    private delegate uint ZwReadVirtualMemory_t(IntPtr intptr_0, IntPtr intptr_1, [Out] byte[] byte_0, int int_0, ref int int_1);

    private delegate int CallNextHookEx_mouse_t(IntPtr intptr_0, int int_0, int int_1, MSLLHOOKSTRUCT struct2_0);

    private delegate uint ZwClose_t(IntPtr intptr_0);

    private static mouse_event_t mouse_event = GetProcAddressManualy<mouse_event_t>("user32.dll", "mouse_event");

    private static LoadLibraryA_t LoadLibraryA = GetProcAddressManualy<LoadLibraryA_t>("kernel32.dll", "LoadLibraryA");

    private static Sleep_t Sleep = GetProcAddressManualy<Sleep_t>("kernel32.dll", "Sleep");

    private static HookCallbackKeyboard_t HookCallbackKeybaordFn = HookCallbackKeybaord;

    private static GlobalLock_t GlobalLock = GetProcAddressManualy<GlobalLock_t>("kernel32.dll", "GlobalLock");

    private static TranslateMessage_t TranslateMessage = GetProcAddressManualy<TranslateMessage_t>("user32.dll", "TranslateMessage");

    private static NtOpenProcess_t NtOpenProcess = GetProcAddressManualy<NtOpenProcess_t>("ntdll.dll", "NtOpenProcess");

    private static VirtualQueryEx_t VirtualQueryEx = GetProcAddressManualy<VirtualQueryEx_t>("kernel32.dll", "VirtualQueryEx");

    private static Module32Next_t Module32Next = GetProcAddressManualy<Module32Next_t>("kernel32.dll", "Module32Next");

    private static GetWindowThreadProcessId_t GetWindowThreadProcessId = GetProcAddressManualy<GetWindowThreadProcessId_t>("user32.dll", "GetWindowThreadProcessId");

    private static GetClipboardData_t GetClipboardData = GetProcAddressManualy<GetClipboardData_t>("user32.dll", "GetClipboardData");

    private static GetForegroundWindow_t GetForegroundWindow = GetProcAddressManualy<GetForegroundWindow_t>("user32.dll", "GetForegroundWindow");

    private static VirtualFree_t VirtualFree = GetProcAddressManualy<VirtualFree_t>("kernel32.dll", "VirtualFree");

    private static GlobalUnlock_t GlobalUnlock = GetProcAddressManualy<GlobalUnlock_t>("kernel32.dll", "GlobalUnlock");

    private static OpenClipboard_t OpenClipboard = GetProcAddressManualy<OpenClipboard_t>("user32.dll", "OpenClipboard");

    private static Module32First_t Module32First = GetProcAddressManualy<Module32First_t>("kernel32.dll", "Module32First");

    private static SetWindowsHookExA_keyboard_t SetWindowsHookExA_keyboard = GetProcAddressManualy<SetWindowsHookExA_keyboard_t>("user32.dll", "SetWindowsHookExA");

    private static Thread SoundEsp_Thread = new Thread(SoundEsp);

    private static IsClipboardFormatAvailable_t IsClipboardFormatAvailable = GetProcAddressManualy<IsClipboardFormatAvailable_t>("user32.dll", "IsClipboardFormatAvailable");

    private static CheatVariables iuxehHelper = new CheatVariables();

    private static CheatConfig cheatConfig = new CheatConfig();

    private static CheatOffsets cheatOffsets = new CheatOffsets();

    private static ChearAimbotVariables cheatAimbotHelper = new ChearAimbotVariables();

    private static ChearTriggerVariables cheatTriggerHelper = new ChearTriggerVariables();

    private static ChearBunyHopVariables cheatBunnyHopHelper = new ChearBunyHopVariables();

    private static EmptyClipboard_t EmptyClipboard = GetProcAddressManualy<EmptyClipboard_t>("user32.dll", "EmptyClipboard");

    private static SetWindowsHookExA_mouse_t SetWindowsHookExA_mouse = GetProcAddressManualy<SetWindowsHookExA_mouse_t>("user32.dll", "SetWindowsHookExA");

    private static CreateProcessAsUserA_t CreateProcessAsUserA = GetProcAddressManualy<CreateProcessAsUserA_t>("advapi32.dll", "CreateProcessAsUserA");

    private static ZwClose_t ZwClose = GetProcAddressManualy<ZwClose_t>("ntdll.dll", "ZwClose");

    private static CreateToolhelp32Snapshot_t CreateToolhelp32Snapshot = GetProcAddressManualy<CreateToolhelp32Snapshot_t>("kernel32.dll", "CreateToolhelp32Snapshot");

    private static Thread GlowEspRadarNoFlash_Thread = new Thread(GlowEspRadarNoFlash);

    private static K32GetMappedFileNameA_t K32GetMappedFileNameA = GetProcAddressManualy<K32GetMappedFileNameA_t>("kernel32.dll", "K32GetMappedFileNameA", "psapi.dll", "GetMappedFileNameA");

    private static CallNextHookEx_keyboard_t CallNextHookEx_keyboard = GetProcAddressManualy<CallNextHookEx_keyboard_t>("user32.dll", "CallNextHookEx");

    private static ZwWriteVirtualMemory_t ZwWriteVirtualMemory = GetProcAddressManualy<ZwWriteVirtualMemory_t>("ntdll.dll", "ZwWriteVirtualMemory");

    private static GetMessageA_t GetMessageA = GetProcAddressManualy<GetMessageA_t>("user32.dll", "GetMessageA");

    private static GetCurrentThreadId_t GetCurrentThreadId = GetProcAddressManualy<GetCurrentThreadId_t>("kernel32.dll", "GetCurrentThreadId");

    private static HookCallbackMouse_t HookCallbackMouseFn = HookCallbackMouse;

    private static SendInput_t SendInput = GetProcAddressManualy<SendInput_t>("user32.dll", "SendInput");

    private static VirtualProtect_t VirtualProtect = GetProcAddressManualy<VirtualProtect_t>("kernel32.dll", "VirtualProtect");

    private static Beep_t Beep = GetProcAddressManualy<Beep_t>("kernel32.dll", "Beep");

    private static ZwReadVirtualMemory_t ZwReadVirtualMemory = GetProcAddressManualy<ZwReadVirtualMemory_t>("ntdll.dll", "ZwReadVirtualMemory");

    private static PostThreadMessageA_t PostThreadMessageA = GetProcAddressManualy<PostThreadMessageA_t>("user32.dll", "PostThreadMessageA");

    private static Thread MouseKeybaordHook_Thread = new Thread(ActiveMouseKeybaordHook);

    private static CallNextHookEx_mouse_t CallNextHookEx_mouse = GetProcAddressManualy<CallNextHookEx_mouse_t>("user32.dll", "CallNextHookEx");

    private static CloseClipboard_t CloseClipboard = GetProcAddressManualy<CloseClipboard_t>("user32.dll", "CloseClipboard");

    private static UnhookWindowsHookEx_t UnhookWindowsHookEx = GetProcAddressManualy<UnhookWindowsHookEx_t>("user32.dll", "UnhookWindowsHookEx");

    private static uint GetLocalPlayer()
    {
        return BitConverter.ToUInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwLocalPlayer, 4), 0);
    }

    private static void RecoilControlSystem(float[] AimPunch, ref float[] lastAimPunchValues, float[] viewAngles, int shotsFired, bool aimbotTargetFound, ref float[] lastAimPunch)
    {
        float recoilVertical = (aimbotTargetFound) ? 2f : (cheatConfig.VerticalRecoil[iuxehHelper.normalWeaponsConfigIndex] / 50f);
        float recoilHorizontal = (aimbotTargetFound) ? 2f : (cheatConfig.HorizontalRecoil[iuxehHelper.normalWeaponsConfigIndex] / 50f);
        bool recoilSuccess = false;
        if (lastAimPunchValues[2] != recoilVertical)
        {
            lastAimPunchValues[2] = recoilVertical;
        }
        if (lastAimPunchValues[3] != recoilHorizontal)
        {
            lastAimPunchValues[3] = recoilHorizontal;
        }
        if (recoilVertical > 0f && shotsFired > 1 && AimPunch[0] != 0f)
        {
            float[] viewAnglesCopy = new float[3];
            viewAngles.CopyTo(viewAnglesCopy, 0);
            float viewAnglesPunchX = lastAimPunchValues[0] * lastAimPunchValues[2] - AimPunch[0] * recoilVertical;
            float viewAnglesPunchY = lastAimPunchValues[1] * lastAimPunchValues[3] - AimPunch[1] * recoilHorizontal;
            if (viewAnglesPunchX != 0f || viewAnglesPunchY != 0f)
            {
                int pitchMove = default;
                int yawMove = default;
                if (FaceitMode())
                {
                    viewAngles[0] += viewAnglesPunchX;
                    viewAngles[1] += viewAnglesPunchY;
                    NormalizeAngles(ref viewAngles);
                    if (SetViewAngles(viewAngles, ref viewAnglesCopy, unUsed: true, ref pitchMove, ref yawMove))
                    {
                        lastAimPunchValues = new float[4]
                        {
                            AimPunch[0],
                            AimPunch[1],
                            recoilVertical,
                            recoilHorizontal
                        };
                    }
                }
                else
                {
                    viewAngles[0] += viewAnglesPunchX;
                    viewAngles[1] += viewAnglesPunchY;
                    NormalizeAngles(ref viewAngles);
                    SetViewAngles(viewAngles, ref viewAnglesCopy, unUsed: true, ref pitchMove, ref yawMove);
                    lastAimPunchValues = new float[4]
                    {
                        AimPunch[0],
                        AimPunch[1],
                        recoilVertical,
                        recoilHorizontal
                    };
                }
                recoilSuccess = true;
            }
        }
        if (shotsFired == 0 && AimPunch[0] == 0f && AimPunch[1] == 0f)
        {
            lastAimPunchValues = new float[4]
            {
                0f,
                0f,
                0f,
                0f
            };
            lastAimPunch = new float[2]
            {
                0f,
                0f
            };
        }
        else if ((AimPunch[0] != 0f || AimPunch[1] != 0f) && (!recoilSuccess))
        {
            lastAimPunchValues = new float[4]
            {
                AimPunch[0],
                AimPunch[1],
                2f,
                2f
            };
        }
    }

    private static bool IsNonZeroVector(float[] vec)
    {
        if (vec[0] == 0f && vec[1] == 0f && vec[2] == 0f)
        {
            return false;
        }
        return true;
    }

    private static float GetRealSensitivity()
    {
        float num = iuxehHelper.Sensitivity;
        if (iuxehHelper.IsScoped || 1 == 0)
        {
            num *= iuxehHelper.floatZoomSensitivity_4;
            switch (iuxehHelper.normalWeaponsConfigIndex)
            {
                case 12:
                case 17:
                    num *= 0.5f;
                    break;
                case 18:
                    switch (iuxehHelper.activeWeaponZoomLevel)
                    {
                        case 1:
                            num *= 4f / 9f;
                            break;
                        case 2:
                            num *= 0.111111112f;
                            break;
                    }
                    break;
                case 19:
                case 20:
                case 21:
                    switch (iuxehHelper.activeWeaponZoomLevel)
                    {
                        case 1:
                            num *= 4f / 9f;
                            break;
                        case 2:
                            num *= 355f / (678f * (float)Math.PI);
                            break;
                    }
                    break;
            }
        }
        return num;
    }

    private static IntPtr GetProcAddress(string dllName, string functionName)
    {
        IntPtr intPtr = default;
        IEnumerator enumerator = default(IEnumerator);
        try
        {
            enumerator = Process.GetCurrentProcess().Modules.GetEnumerator();
            while (enumerator.MoveNext() ? true : false)
            {
                ProcessModule processModule = (ProcessModule)enumerator.Current;
                if (Operators.CompareString(processModule.ModuleName.ToLower(), dllName, TextCompare: false) == 0)
                {
                    intPtr = processModule.BaseAddress;
                }
            }
        }
        finally
        {
            if (enumerator is IDisposable)
            {
                (enumerator as IDisposable).Dispose();
            }
        }
        IntPtr intPtr2 = default;
        if (intPtr == intPtr2)
        {
            intPtr = LoadLibraryA(dllName);
        }
        byte[] byte_ = ReadInternalMemory(AddToPointer(intPtr, BitConverter.ToUInt32(ReadInternalMemory(AddToPointer(intPtr, BitConverter.ToUInt32(ReadInternalMemory(AddToPointer(intPtr, 60L), 4), 0) + 136L), 4), 0) + 24L), 16);
        int num = BitConverter.ToInt32(byte_, 0);
        int num2 = 0;
        ushort num4;
        while (true)
        {
            if (num2 <= num)
            {
                uint num3 = BitConverter.ToUInt32(ReadInternalMemory(AddToPointer(AddToPointer(intPtr, num2 * 4), BitConverter.ToUInt32(byte_, 8)), 4), 0);
                string left = Encoding.ASCII.GetString(ReadInternalMemory(AddToPointer(intPtr, num3), 64)).Split('\0')[0];
                num4 = BitConverter.ToUInt16(ReadInternalMemory(AddToPointer(AddToPointer(intPtr, num2 * 2), BitConverter.ToUInt32(byte_, 12)), 2), 0);
                if (Operators.CompareString(left, functionName, TextCompare: false) == 0)
                {
                    break;
                }
                num2++;
                continue;
            }
            return intPtr2;
        }
        return AddToPointer(intPtr, BitConverter.ToUInt32(ReadInternalMemory(AddToPointer(AddToPointer(intPtr, num4 * 4), BitConverter.ToUInt32(byte_, 4)), 4), 0));
    }

    private static bool FaceitMode()
    {
        if (!cheatConfig.FaceitMode && !iuxehHelper.is5EClient)
        {
            return false;
        }
        return true;
    }

    private static bool IsKeyPressed(uint vkey)
    {
        lock (iuxehHelper.keyState)
        {
            return iuxehHelper.keyState[vkey];
        }
    }

    private static int HookCallbackKeybaord(int nCode, int wParam, KBDLLHOOKSTRUCT kbdStruct)
    {
        if (nCode == 0)
        {
            switch (wParam)
            {
                case 256:
                case 260:
                    SetKeyStateWrapper(kbdStruct.vkCode, val: true);
                    OnKeyPressed(kbdStruct.vkCode);
                    break;
                case 257:
                case 261:
                    SetKeyStateWrapper(kbdStruct.vkCode, val: false);
                    break;
            }
        }
        return CallNextHookEx_keyboard(iuxehHelper.keyboardHookHandle, nCode, wParam, kbdStruct);
    }

    private static byte[] ReadCSGOMemory(uint pointer, uint offset, int size)
    {
        byte[] buffer = new byte[size];
        if (pointer > 0L)
        {
            int bytesRead = 0;
            ZwReadVirtualMemory(iuxehHelper.csgoHandle, new IntPtr(pointer + offset), buffer, size, ref bytesRead);
        }
        return buffer;
    }

    private static float GetFov(ref float[] sourcePosition, ref float[] destPosition, ref float distance)
    {
        if (sourcePosition[0] == destPosition[0] && sourcePosition[1] == destPosition[1])
        {
            return 0f;
        }
        double num = sourcePosition[0] - destPosition[0];
        double num2 = sourcePosition[1] - destPosition[1];
        if (num > 180.0)
        {
            num -= (double)(float)(Math.Round(num / 360.0, MidpointRounding.AwayFromZero) * 360.0);
        }
        else if (num < -180.0)
        {
            num += (double)(float)(Math.Round(num / 360.0, MidpointRounding.AwayFromZero) * -360.0);
        }
        if (num2 > 180.0)
        {
            num2 -= (double)(float)(Math.Round(num2 / 360.0, MidpointRounding.AwayFromZero) * 360.0);
        }
        else if (num2 < -180.0)
        {
            num2 += (double)(float)(Math.Round(num2 / 360.0, MidpointRounding.AwayFromZero) * -360.0);
        }
        if (num < 0.0)
        {
            num *= -1.0;
        }
        if (num2 < 0.0)
        {
            num2 *= -1.0;
        }
        float num3 = (float)Math.Pow(Math.Pow(num * 0.01745329252 * (double)distance, 2.0) + Math.Pow(num2 * 0.01745329252 * (double)distance, 2.0), 0.5) * 2.2f;
        if ((!float.IsNaN(num3)) && ((!float.IsInfinity(num3))))
        {
            return num3;
        }
        return 999f;
    }

    private static int GetRandom(int min, int max)
    {
        return new Random(Guid.NewGuid().GetHashCode()).Next(min, max);
    }

    private static long TryLoadSettingsFromClipboard()
    {
        try
        {
            string[] array = Strings.Split(GetClipboardText());
            if (array.Length == 699)
            {
                LoadConfigFromArray(array);
                Beep(350u, 140u);
                Beep(350u, 140u);
                Beep(350u, 140u);
                EmptyClipboard();
                iuxehHelper.panicButtonActive = true;
                Sleep(500);
                iuxehHelper.panicButtonActive = false;
            }
            CloseClipboard();
        }
        catch (Exception projectError)
        {
            ProjectData.SetProjectError(projectError);
            long result = 0L;
            ProjectData.ClearProjectError();
            return result;
        }
        return iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds;
    }

    private static int GetWeaponConfigIndex()
    {
        uint activeWeaponHandle = BitConverter.ToUInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_hActiveWeapon, 4), 0);
        if (activeWeaponHandle != 0L)
        {
            uint activeWeaponPointer = BitConverter.ToUInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, (uint)(cheatOffsets.dwEntityList + (activeWeaponHandle & 0xFFFL) * 16L - 16L), 4), 0);
            iuxehHelper.activeWeaponZoomLevel = BitConverter.ToInt32(ReadCSGOMemory(activeWeaponPointer, 13264u, 4), 0); //m_zoomLevel
            if (activeWeaponPointer != 0L)
            {
                int configIndex = GetWeaponConfigIndex(BitConverter.ToInt16(ReadCSGOMemory(activeWeaponPointer, cheatOffsets.m_iItemDefinitionIndex, 2), 0));
                if (configIndex != 0)
                {
                    return configIndex;
                }
            }
        }
        return 0;
    }

    private static byte[] ReadInternalMemory(IntPtr pointer, int size)
    {
        byte[] buffer = new byte[size];
        Marshal.Copy(pointer, buffer, 0, size);
        return buffer;
    }

    private static int GetWeaponConfigIndex(short itemDefIndex)
    {
        switch (itemDefIndex)
        {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 30:
                return 5;
            case 32:
                return 6;
            case 36:
                return 7;
            case 61:
                return 8;
            case 63:
                return 9;
            case 64:
                return 10;
            case 7:
                return 11;
            case 8:
                return 12;
            case 10:
                return 13;
            case 13:
                return 14;
            case 60:
                return 15;
            case 16:
                return 16;
            case 39:
                return 17;
            case 9:
                return 18;
            case 11:
                return 19;
            case 38:
                return 20;
            case 40:
                return 21;
            case 27:
                return 22;
            case 35:
                return 23;
            case 29:
                return 24;
            case 25:
                return 25;
            case 26:
                return 26;
            case 17:
                return 27;
            case 23:
                return 28;
            case 33:
                return 29;
            case 34:
                return 30;
            case 19:
                return 31;
            case 24:
                return 32;
            case 14:
                return 33;
            case 28:
                return 34;
            case 31:
                return 36;
            default:
                return 0;
            case 42:
            case 59:
            case 500:
            case 505:
            case 506:
            case 507:
            case 508:
            case 509:
            case 512:
            case 514:
            case 515:
            case 516:
                return 35;
        }
    }

    private static void OnKeyPressed(uint vkey)
    {
        if (vkey == cheatConfig.AimbotKey[iuxehHelper.normalWeaponsConfigIndex])
        {
            if (!cheatAimbotHelper.AimbotIsrunning && iuxehHelper.cheatIsRunning)
            {
                Thread aimbotThread = new Thread(AimbotRcs);
                aimbotThread.Start();
                cheatAimbotHelper.AimbotIsrunning = true;
            }
        }
        else if (vkey == 1)
        {
            if (!cheatAimbotHelper.AimbotIsrunning && iuxehHelper.cheatIsRunning)
            {
                Thread aimbotThread = new Thread(AimbotRcs);
                aimbotThread.Start();
                cheatAimbotHelper.AimbotIsrunning = true;
            }
        }
        else if (vkey == cheatConfig.TriggerKeybind[iuxehHelper.specialWeaponConfigIndex])
        {
            if (!FaceitMode() && !cheatTriggerHelper.TriggerIsrunning && iuxehHelper.cheatIsRunning)
            {
                Thread triggerThread = new Thread(TrigggerBot);
                triggerThread.Start();
                cheatTriggerHelper.TriggerIsrunning = true;
            }
        }
        else if (vkey == cheatConfig.TriggerKeybind[iuxehHelper.normalWeaponsConfigIndex])
        {
            if (!FaceitMode())
            {
                if (!cheatAimbotHelper.AimbotIsrunning && iuxehHelper.cheatIsRunning)
                {
                    Thread aimbotThread = new Thread(AimbotRcs);
                    aimbotThread.Start();
                    cheatAimbotHelper.AimbotIsrunning = true;
                }
                if (!cheatTriggerHelper.TriggerIsrunning && iuxehHelper.cheatIsRunning)
                {
                    Thread triggerThread = new Thread(TrigggerBot);
                    triggerThread.Start();
                    cheatTriggerHelper.TriggerIsrunning = true;
                }
            }
        }
        else if (vkey == cheatConfig.BhopKey && !FaceitMode() && !cheatBunnyHopHelper.BunnyhopActive && iuxehHelper.cheatIsRunning)
        {
            Thread bunnyHopThread = new Thread(BunnyHop);
            bunnyHopThread.Start();
            cheatBunnyHopHelper.BunnyhopActive = true;
        }
    }

    private static void MainLoop()
    {
        Beep(400u, 170u);
        Beep(250u, 170u);
        Thread updateThread = new Thread(UpdateThread);
        MouseKeybaordHook_Thread = new Thread(ActiveMouseKeybaordHook);
        MouseKeybaordHook_Thread.Start();
        long nextCSGOModuleScan = 0L;
        //long lastLoadSettingsCheck = 0L;
        long lastLoadSettingsCheck = -100000L;
        while (true)
        {
            if (lastLoadSettingsCheck + 1000L < iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds)
            {
                lastLoadSettingsCheck = TryLoadSettingsFromClipboard();
            }
            if (nextCSGOModuleScan < iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds)
            {
                nextCSGOModuleScan = iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds + 2500L;
                Process[] processes = Process.GetProcesses();
                Process process_ = null;
                var foundCSGO = (FindProcessInList(processes, "csgo", ref process_) != 0);
                var foundEasyAntiCheat = (FindProcessInList(processes, "easyanticheat", ref process_) != 0);
                var found5EClient = (FindProcessInList(processes, "5EClient", ref process_) != 0);
                if ((foundCSGO || foundEasyAntiCheat) && !iuxehHelper.modulesLoaded)
                {
                    Sleep(5000);
                    Beep(250u, 170u);
                    if (found5EClient)
                    {
                        iuxehHelper.is5EClient = true;
                        cheatOffsets.m_dwBoneMatrix = 10528u;
                    }
                    else
                    {
                        cheatOffsets.m_dwBoneMatrix = 9896u;
                    }
                    if (foundEasyAntiCheat)
                    {
                        Sleep(3000);
                        var csgoPath = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Counter-Strike Global Offensive\\csgo.exe";
                        PROCESS_INFORMATION processInformation = default;
                        PSTARTUPINFOA startupInfo = default;
                        startupInfo.cb = (uint)Marshal.SizeOf(typeof(PSTARTUPINFOA));
                        CreateProcessAsUserA(default, csgoPath, "\"" + csgoPath + "\" -steam", IntPtr.Zero, IntPtr.Zero, bool_0: false, 0u, IntPtr.Zero, Path.GetDirectoryName(csgoPath) + "\\", ref startupInfo, ref processInformation);
                        iuxehHelper.csgoHandle = processInformation.hProcess;
                        iuxehHelper.csgoPID = processInformation.dwProcessId;
                    }
                    else if (foundCSGO)
                    {
                        process_ = null;
                        iuxehHelper.csgoPID = (uint)FindProcessInList(processes, "csgo", ref process_);
                        iuxehHelper.csgoHandle = OpenProcess(1080u, iuxehHelper.csgoPID);
                    }
                    uint client_size = 0u;
                    uint engine_size = 0u;
                    bool tryBruteFroce = false;
                    while (iuxehHelper.clientBaseAddress == 0 || iuxehHelper.engineBaseAddress == 0)
                    {
                        if (tryBruteFroce)
                        {
                            iuxehHelper.clientBaseAddress = GetModuleBaseByHandle(iuxehHelper.csgoHandle, "client.dll", ref client_size);
                            iuxehHelper.engineBaseAddress = GetModuleBaseByHandle(iuxehHelper.csgoHandle, "engine.dll", ref engine_size);
                        }
                        else
                        {
                            iuxehHelper.clientBaseAddress = GetModuleBaseByPID(iuxehHelper.csgoPID, "client.dll", ref client_size);
                            iuxehHelper.engineBaseAddress = GetModuleBaseByPID(iuxehHelper.csgoPID, "engine.dll", ref engine_size);
                        }
                        Sleep(2000);
                        tryBruteFroce = !tryBruteFroce;
                    }
                    InitPatternScanner(iuxehHelper.clientBaseAddress, client_size, iuxehHelper.engineBaseAddress, engine_size);
                    iuxehHelper.modulesLoaded = true;
                    Beep(400u, 170u);
                }
                else if (!foundCSGO && iuxehHelper.modulesLoaded)
                {
                    iuxehHelper.modulesLoaded = false;
                    if (!(iuxehHelper.csgoHandle == default))
                    {
                        ZwClose(iuxehHelper.csgoHandle);
                    }
                    iuxehHelper.csgoHandle = default;
                    iuxehHelper.csgoPID = 0u;
                    iuxehHelper.clientBaseAddress = 0u;
                    iuxehHelper.engineBaseAddress = 0u;
                }
            }
            if (IsKeyPressed(cheatConfig.PanicKey))
            {
                SetKeyStateWrapper(cheatConfig.PanicKey, val: false);
                iuxehHelper.panicButtonActive = true;
                Beep(350u, 140u);
                if (!iuxehHelper.modulesLoaded)
                {
                    if (iuxehHelper.mouse_keyboardHookThreadID != 0L)
                    {
                        PostThreadMessageA(iuxehHelper.mouse_keyboardHookThreadID, 18u, 0, default);
                    }
                    iuxehHelper.cheatIsRunning = false;
                    Environment.Exit(0);
                }
                while ((!IsKeyPressed(cheatConfig.PanicKey)))
                {
                    Sleep(100);
                }
                iuxehHelper.panicButtonActive = false;
                Beep(350u, 140u);
                Beep(350u, 140u);
                Sleep(500);
            }
            if (iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds - 259200000L > iuxehHelper.startTime)
            {
                if (iuxehHelper.mouse_keyboardHookThreadID != 0L)
                {
                    PostThreadMessageA(iuxehHelper.mouse_keyboardHookThreadID, 18u, 0, default);
                }
                iuxehHelper.cheatIsRunning = false;
                Environment.Exit(0);
            }
            if (!iuxehHelper.cheatIsRunning && iuxehHelper.modulesLoaded && GetForegroundWindowProcessID() == iuxehHelper.csgoPID && !iuxehHelper.panicButtonActive && IsInGame() && !updateThread.IsAlive)
            {
                updateThread = new Thread(UpdateThread);
                updateThread.Start();
            }
            Sleep(500);
        }
    }

    private static void GlowEspRadarNoFlash()
    {
        try
        {
            var glowColors = new float[5, 4]
            {
                {
                    0.8235294f,
                    41f / 51f,
                    8f / 51f,
                    0.6f
                },
                {
                    112f / 255f,
                    71f / 255f,
                    52f / 85f,
                    0.6f
                },
                {
                    23f / 85f,
                    182f / 255f,
                    73f / 255f,
                    0.6f
                },
                {
                    89f / 255f,
                    199f / 255f,
                    44f / 51f,
                    0.6f
                },
                {
                    248f / 255f,
                    149f / 255f,
                    29f / 255f,
                    0.6f
                }
            };
            byte[] defaultColor = new byte[16]
            {
                0,
                0,
                128,
                63,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                154,
                153,
                25,
                63
            };
            uint playerResources = BitConverter.ToUInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwPlayerResource, 4), 0);
            int CompTeammateColor = 0;
            uint GlowManagerPointer = 0u;
            int EntityGlowIndex = 0;
            float flashAmount = 0f;
            while (iuxehHelper.cheatIsRunning && (cheatConfig.GlowESPKeybind > 0L || cheatConfig.RadarEnabled || !(cheatConfig.NoFlashAmount <= 0f)))
            {
                if (cheatConfig.GlowESPKeybind > 0L)
                {
                    if (IsKeyPressed(cheatConfig.GlowESPKeybind))
                    {
                        iuxehHelper.isGlowActive = (!iuxehHelper.isGlowActive);
                        while (IsKeyPressed(cheatConfig.GlowESPKeybind))
                        {
                            Sleep(100);
                        }
                    }
                    if (iuxehHelper.isGlowActive)
                    {
                        playerResources = BitConverter.ToUInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwPlayerResource, 4), 0);

                        foreach (var entity in GetValidTargets())
                        {
                            byte[] Color = new byte[16];
                            CompTeammateColor = BitConverter.ToInt32(ReadCSGOMemory(playerResources, (uint)(7380 + entity.Index * 4), 4), 0); //DT_CSPlayerResource => m_iCompTeammateColor + 0x4
                            if (CompTeammateColor >= 0 && CompTeammateColor <= 4)
                            {
                                Buffer.BlockCopy(BitConverter.GetBytes(glowColors[CompTeammateColor, 0]), 0, Color, 0, 4);
                                Buffer.BlockCopy(BitConverter.GetBytes(glowColors[CompTeammateColor, 1]), 0, Color, 4, 4);
                                Buffer.BlockCopy(BitConverter.GetBytes(glowColors[CompTeammateColor, 2]), 0, Color, 8, 4);
                                Buffer.BlockCopy(BitConverter.GetBytes(glowColors[CompTeammateColor, 3]), 0, Color, 12, 4);
                            }
                            else
                                Buffer.BlockCopy(defaultColor, 0, Color, 0, 16);

                            GlowManagerPointer = BitConverter.ToUInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwGlowObjectManager, 4), 0);
                            EntityGlowIndex = BitConverter.ToInt32(ReadCSGOMemory(entity.EntityBase, cheatOffsets.m_iGlowIndex, 4), 0);
                            WriteCSGOMemory(GlowManagerPointer, (uint)(EntityGlowIndex * 56 + 4), defaultColor);
                            WriteCSGOMemory(GlowManagerPointer, (uint)(EntityGlowIndex * 56 + 36), new byte[1]
                            {
                                    255
                            });
                        }

                    }
                }
                if (cheatConfig.RadarEnabled)
                {
                    foreach (var entity in GetValidTargets())
                    {
                        if (BitConverter.ToInt32(ReadCSGOMemory(entity.EntityBase, cheatOffsets.m_bSpotted, 4), 0) == 0)
                        {
                            WriteCSGOMemory(entity.EntityBase, cheatOffsets.m_bSpotted, BitConverter.GetBytes(1));
                        }
                    }

                }
                if (cheatConfig.NoFlashAmount > 0f)
                {
                    flashAmount = 255f - cheatConfig.NoFlashAmount / 100f * 255f;
                    if (BitConverter.ToSingle(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_flFlashMaxAlpha, 4), 0) != flashAmount)
                    {
                        WriteCSGOMemory(GetLocalPlayer(), cheatOffsets.m_flFlashMaxAlpha, BitConverter.GetBytes(flashAmount));
                    }
                }
                Sleep(1);
            }
        }
        catch (Exception projectError)
        {
            ProjectData.SetProjectError(projectError);
            ProjectData.ClearProjectError();
        }
    }

    private static void SetKeyStateWrapper(uint vkey, bool val)
    {
        SetKeyState(vkey, val);
    }

    private static int GetActiveWeaponConfigType()
    {
        int activeWeaponHandle = BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_hActiveWeapon, 4), 0);
        uint activeWeaponPtr = BitConverter.ToUInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, (uint)(cheatOffsets.dwEntityList + (activeWeaponHandle & 0xFFF) * 16 - 16L), 4), 0);
        int weaponConfigIndex = GetWeaponConfigIndex(BitConverter.ToInt16(ReadCSGOMemory(activeWeaponPtr, cheatOffsets.m_iItemDefinitionIndex, 2), 0));
        if (weaponConfigIndex != 0 && BitConverter.ToInt32(ReadCSGOMemory(activeWeaponPtr, cheatOffsets.m_bInReload, 4), 0) != 1)
        {
            if (weaponConfigIndex <= 34 && BitConverter.ToInt32(ReadCSGOMemory(activeWeaponPtr, cheatOffsets.m_iClip1, 4), 0) > 0)
            {
                return 1;
            }
            if (weaponConfigIndex > 34 && weaponConfigIndex < 37)
            {
                return 2;
            }
        }
        return 0;
    }

    private static void MouseMove(int x, int y)
    {
        INPUT[] inputs = new INPUT[1];
        inputs[0].type = 0;
        inputs[0].mi.dx = x;
        inputs[0].mi.dy = y;
        inputs[0].mi.dwFlags = 1u;
        SendInput(1u, inputs, 40);
    }

    private static short GetLocalPlayerTeamNum()
    {
        return BitConverter.ToInt16(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_iTeamNum, 2), 0);
    }

    private static int FindProcessInList(Process[] processList, string processName, ref Process targetProcess)
    {
        int num = 0;
        Process process;
        targetProcess = null;
        while (true)
        {
            if (num < processList.Length)
            {
                process = processList[num];
                if (Operators.CompareString(process.ProcessName.ToLower(), processName.ToLower(), TextCompare: false) == 0)
                {
                    break;
                }
                num++;
                continue;
            }
            return 0;
        }
        targetProcess = process;
        return process.Id;
    }

    private static int GetSelectedBone()
    {
        if (cheatConfig.AimbotBoneSeletion[iuxehHelper.normalWeaponsConfigIndex] == 2) //BoneSelection - Random
        {
            int highestIndex = 0;
            int boneConfigIndex = 0;
            do
            {
                if (cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneConfigIndex] != 0)
                {
                    highestIndex++;
                }
                boneConfigIndex++;
            }
            while (boneConfigIndex <= 3);
            int randomIndex = GetRandom(0, highestIndex + 1);
            highestIndex = 0;
            int boneRandomIndex = 0;
            do
            {
                if (cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneRandomIndex] != 0)
                {
                    if (highestIndex == randomIndex)
                    {
                        return cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneRandomIndex];
                    }
                    highestIndex++;
                }
                boneRandomIndex++;
            }
            while (boneRandomIndex <= 3);
        }
        return 0;
    }

    private static T GetProcAddressManualy<T>(string dllName, string functionName, string dllName2 = null, string functionName2 = null)
    {
        IntPtr intPtr = GetProcAddress(dllName, functionName);
        IntPtr value = default;
        if (intPtr == value || 1 == 0)
        {
            intPtr = GetProcAddress(dllName2, functionName2);
        }
        return (T)(object)Marshal.GetDelegateForFunctionPointer(intPtr, typeof(T));
    }

    private static float GetClosestDistanceToTarget()
    {
        float closestTarget = float.MaxValue;
        byte[] positionBuffer = ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_vecOrigin, 12);
        float[] localplayerPosition = new float[3]
        {
            BitConverter.ToSingle(positionBuffer, 0),
            BitConverter.ToSingle(positionBuffer, 4),
            BitConverter.ToSingle(positionBuffer, 8)
        };

        foreach (var current in GetValidTargets())
        {
            float distanceToPlayer = DistanceBetween(localplayerPosition, current.vecOrigin);
            if (distanceToPlayer < closestTarget)
            {
                closestTarget = distanceToPlayer;
            }
        }
        return closestTarget;

    }

    private static float[] GetLocalPosition()
    {
        byte[] buffer = ReadCSGOMemory(iuxehHelper.engineBaseAddress, cheatOffsets.dwLocalPosition, 12);
        return new float[3]
        {
            BitConverter.ToSingle(buffer, 0),
            BitConverter.ToSingle(buffer, 4),
            BitConverter.ToSingle(buffer, 8)
        };
    }

    private static uint GetForegroundWindowProcessID()
    {
        IntPtr processID = default;
        GetWindowThreadProcessId(GetForegroundWindow(), ref processID);
        return (uint)(int)processID;
    }

    private static bool IsCurrentWeaponAutomatic()
    {
        if (iuxehHelper.normalWeaponsConfigIndex == 9)
        {
            return true;
        }
        if (iuxehHelper.normalWeaponsConfigIndex >= 11 && iuxehHelper.normalWeaponsConfigIndex <= 17)
        {
            return true;
        }
        if (iuxehHelper.normalWeaponsConfigIndex >= 18 && iuxehHelper.normalWeaponsConfigIndex <= 21)
        {
            return true;
        }
        if (iuxehHelper.normalWeaponsConfigIndex >= 26 && iuxehHelper.normalWeaponsConfigIndex <= 32)
        {
            return true;
        }
        if (iuxehHelper.normalWeaponsConfigIndex >= 33 && iuxehHelper.normalWeaponsConfigIndex <= 34)
        {
            return true;
        }
        return false;
    }

    private static List<BoneInfo> GetBonesPositions(uint boneMatrix, EntityInfo entity)
    {
        List<BoneInfo> list = new List<BoneInfo>();
        int maxBoneID = 0;
        int boneItterator = 0;
        do
        {
            if (cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator] != 0 && cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator] > maxBoneID)
            {
                maxBoneID = cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator];
            }
            boneItterator++;
        }
        while (boneItterator <= 3);
        int minBonID = maxBoneID;
        boneItterator = 0;
        do
        {
            if (cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator] != 0 && cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator] < minBonID)
            {
                minBonID = cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator];
            }
            boneItterator++;
        }
        while (boneItterator <= 3);
        byte[] boneInfo = ReadCSGOMemory(boneMatrix + 12, (uint)(48 * minBonID), 48 * maxBoneID - 48 * minBonID + 36);
        boneItterator = 0;
        do
        {
            if (cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator] != 0)
            {
                list.Add(new BoneInfo
                {
                    X = BitConverter.ToSingle(boneInfo, 48 * cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator] - 48 * minBonID),
                    Y = BitConverter.ToSingle(boneInfo, 48 * cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator] - 48 * minBonID + 16),
                    Z = BitConverter.ToSingle(boneInfo, 48 * cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator] - 48 * minBonID + 32),
                    boneID = cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneItterator]
                });
            }
            boneItterator++;
        }
        while (boneItterator <= 3);
        return list;
    }

    private static bool IsLocalPlayerAlive()
    {
        if (ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_lifeState, 1)[0] == 0 && BitConverter.ToInt32(BitConverter.GetBytes(cheatOffsets.dwMouseEnable ^ BitConverter.ToInt32(ReadCSGOMemory(cheatOffsets.dwMouseEnable, 48u, 4), 0)), 0) != 0)
        {
            return true;
        }
        return false;
    }

    private static bool IsTriggerKeyPressed()
    {
        if (IsKeyPressed(cheatConfig.TriggerKeybind[iuxehHelper.normalWeaponsConfigIndex]))
        {
            return true;
        }
        if (IsKeyPressed(cheatConfig.TriggerKeybind[iuxehHelper.specialWeaponConfigIndex]))
        {
            return true;
        }
        return false;
    }

    private static float[] GetBonePosition(int boneIndex, uint boneMatrix, EntityInfo entity)
    {
        byte[] boneMatrixBuffer = ReadCSGOMemory(boneMatrix + 12, (uint)(48 * boneIndex), 36);
        return new float[3]
        {
            BitConverter.ToSingle(boneMatrixBuffer, 0),
            BitConverter.ToSingle(boneMatrixBuffer, 16),
            BitConverter.ToSingle(boneMatrixBuffer, 32)
        };
    }

    private static void SoundEsp()
    {
        try
        {
            float distance = 0f;
            while ((iuxehHelper.cheatIsRunning) && (cheatConfig.SoundESPEnabled))
            {
                if ((IsLocalPlayerAlive()) && (cheatConfig.SoundESPEnabled))
                {
                    distance = GetClosestDistanceToTarget();
                    if (distance < cheatConfig.SoundESPMaxRange)
                    {
                        if (distance > 15f)
                            SoundESPBeep((int)Math.Round((distance / 100f + 2f) * 70f));
                        else
                            SoundESPBeep(150);
                    }
                }
                Sleep(20);
            }
        }
        catch (Exception projectError)
        {
            ProjectData.SetProjectError(projectError);
            TryStartCheatThreads();
            ProjectData.ClearProjectError();
        }
    }

    private static float DistanceBetween(float[] vec1, float[] vec2)
    {
        double[] delta = new double[3]
        {
            vec2[0] - vec1[0],
            vec2[1] - vec1[1],
            vec2[2] - vec1[2]
        };
        return (float)Math.Sqrt(Math.Pow(delta[0], 2.0) + Math.Pow(delta[1], 2.0) + Math.Pow(delta[2], 2.0));
    }

    private static int TryParseIntegerFromString(string str)
    {
        if (!int.TryParse(str, NumberStyles.Integer, CultureInfo.InvariantCulture, out int result))
        {
            return 0;
        }
        return result;
    }

    private static bool FoundAimBotTarget(ref float[] viewAngles, ref float[] lastAimPunch, ref float targetFov, ref float[] targetAimAngles, ref int selectedBone, ref uint targetBase, ref int lastBoneIndex, bool stickTarget, float aimbotFov, bool aimThroughWalls)
    {
        float[] aimViewAngles = new float[3];
        bool isVisible = false;
        float[] localPosition = GetLocalPosition();
        int targetboneIndex = 0;
        foreach (var current in GetValidTargets())
        {
            int currentBone = 0;
            uint boneMatrix = BitConverter.ToUInt32(ReadCSGOMemory(current.EntityBase, cheatOffsets.m_dwBoneMatrix, 4), 0);
            float aimAnglesFov = float.MaxValue;
            if (lastBoneIndex != 0)
            {
                currentBone = lastBoneIndex;
                float[] bonePosition = GetBonePosition(lastBoneIndex, boneMatrix, current);
                if (IsNonZeroVector(bonePosition))
                {
                    float distance = DistanceBetween(localPosition, bonePosition);
                    CalcAimAngles(ref aimViewAngles, ref localPosition, ref bonePosition, ref lastAimPunch, ref viewAngles, ref aimAnglesFov, ref distance);
                }
            }
            else if (selectedBone == 0)
            {
                int selectedBoneCount = 0;
                int boneIttertor = 0;
                do
                {
                    if (cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneIttertor] != 0)
                    {
                        selectedBoneCount++;
                    }
                    boneIttertor++;
                }
                while (boneIttertor <= 3);
                if (selectedBoneCount == 1)
                {
                    boneIttertor = 0;
                    do
                    {
                        if (cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneIttertor] != 0)
                        {
                            float[] bonePosition = GetBonePosition(cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneIttertor], boneMatrix, current);
                            if (IsNonZeroVector(bonePosition))
                            {
                                float distance = DistanceBetween(localPosition, bonePosition);
                                if (CalcAimAngles(ref aimViewAngles, ref localPosition, ref bonePosition, ref lastAimPunch, ref viewAngles, ref aimAnglesFov, ref distance))
                                {
                                    currentBone = cheatConfig.AimbotBone[iuxehHelper.normalWeaponsConfigIndex, boneIttertor];
                                }
                            }
                        }
                        boneIttertor++;
                    }
                    while (boneIttertor <= 3);
                }
                else
                {
                    foreach (var boneInfo in GetBonesPositions(boneMatrix, current))
                    {
                        float[] bonePosition = new float[3]
                        {
                                    boneInfo.X,
                                    boneInfo.Y,
                                    boneInfo.Z
                        };
                        if (IsNonZeroVector(bonePosition) || 1 == 0)
                        {
                            float distance = DistanceBetween(localPosition, bonePosition);
                            if (CalcAimAngles(ref aimViewAngles, ref localPosition, ref bonePosition, ref lastAimPunch, ref viewAngles, ref aimAnglesFov, ref distance))
                            {
                                currentBone = boneInfo.boneID;
                            }
                        }
                    }
                }
            }
            else
            {
                float[] bonePosition = GetBonePosition(selectedBone, boneMatrix, current);
                if (IsNonZeroVector(bonePosition))
                {
                    float distance = DistanceBetween(localPosition, bonePosition);
                    if (CalcAimAngles(ref aimViewAngles, ref localPosition, ref bonePosition, ref lastAimPunch, ref viewAngles, ref aimAnglesFov, ref distance))
                    {
                        currentBone = selectedBone;
                    }
                }
            }
            if (!(aimAnglesFov < aimbotFov))
            {
                continue;
            }
            if (((!stickTarget)) || targetBase != current.EntityBase)
            {
                if (aimAnglesFov < targetFov)
                {
                    targetFov = aimAnglesFov;
                    targetAimAngles = aimViewAngles;
                    targetBase = current.EntityBase;
                    targetboneIndex = currentBone;
                    if (aimThroughWalls)
                    {
                        isVisible = true;
                    }
                    else if ((!aimThroughWalls) && (IsSpottedByMask(BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), 100u, 4), 0) - 1, ReadCSGOMemory(current.EntityBase, cheatOffsets.m_bSpottedByMask, 4), current.EntityBase)))
                    {
                        isVisible = true;
                    }
                }
                continue;
            }
            targetFov = aimAnglesFov;
            targetAimAngles = aimViewAngles;
            targetBase = current.EntityBase;
            targetboneIndex = currentBone;
            isVisible = true;
            break;
        }

        if (isVisible)
        {
            iuxehHelper.lastTargetBase = targetBase;
            lastBoneIndex = targetboneIndex;
            return true;
        }
        return false;
    }

    private static bool WriteCSGOMemory(uint pointer, uint offset, byte[] buffer)
    {
        try
        {
            if (pointer <= 0L)
                return false;

            int bytesWritten = 0;
            ZwWriteVirtualMemory(iuxehHelper.csgoHandle, new IntPtr(pointer + offset), buffer, buffer.Length, ref bytesWritten);
        }
        catch (Exception projectError)
        {
            ProjectData.SetProjectError(projectError);
            bool result = false;
            ProjectData.ClearProjectError();
            return result;
        }
        return true;
    }

    private static void TryStartCheatThreads()
    {
        if (!SoundEsp_Thread.IsAlive && !FaceitMode())
        {
            SoundEsp_Thread = new Thread(SoundEsp);
            SoundEsp_Thread.Start();
        }
        if (!GlowEspRadarNoFlash_Thread.IsAlive && !FaceitMode())
        {
            GlowEspRadarNoFlash_Thread = new Thread(GlowEspRadarNoFlash);
            GlowEspRadarNoFlash_Thread.Start();
        }
        if (!MouseKeybaordHook_Thread.IsAlive)
        {
            MouseKeybaordHook_Thread = new Thread(ActiveMouseKeybaordHook);
            MouseKeybaordHook_Thread.Start();
        }
    }

    private static void ActiveMouseKeybaordHook()
    {
        try
        {
            iuxehHelper.mouse_keyboardHookThreadID = GetCurrentThreadId();
            iuxehHelper.mouseHookHandle = SetWindowsHookExA_mouse(14, HookCallbackMouseFn, default, 0u);
            iuxehHelper.keyboardHookHandle = SetWindowsHookExA_keyboard(13, HookCallbackKeybaordFn, default, 0u);
            tagMSG msg = default;
            while (GetMessageA(out msg, default, 0u, 0u) ? true : false)
            {
                TranslateMessage(ref msg);
                TranslateMessage(ref msg);
                if (msg.message == 18L)
                {
                    break;
                }
            }
        }
        catch (Exception projectError)
        {
            ProjectData.SetProjectError(projectError);
            UnhookWindowsHookEx(iuxehHelper.mouseHookHandle);
            UnhookWindowsHookEx(iuxehHelper.keyboardHookHandle);
            iuxehHelper.mouse_keyboardHookThreadID = 0u;
            ProjectData.ClearProjectError();
        }
        UnhookWindowsHookEx(iuxehHelper.mouseHookHandle);
        UnhookWindowsHookEx(iuxehHelper.keyboardHookHandle);
        iuxehHelper.mouse_keyboardHookThreadID = 0u;
    }

    private static uint TryParseUintFromString(string str)
    {
        if (!uint.TryParse(str, NumberStyles.Integer, CultureInfo.InvariantCulture, out uint result))
        {
            return 0u;
        }
        return result;
    }

    private static void NormalizeAngles(ref float[] angle)
    {
        while (angle[0] > 180f)
        {
            angle[0] = angle[0] - 360f;
        }
        while (angle[0] < -180f)
        {
            angle[0] = angle[0] + 360f;
        }
        while (angle[1] > 180f)
        {
            angle[1] = angle[1] - 360f;
        }
        while (angle[1] < -180f)
        {
            angle[1] = angle[1] + 360f;
        }
    }

    private static bool ClampAngles(ref float[] angles)
    {
        if ((!float.IsNaN(angles[1])) && ((!float.IsInfinity(angles[1]))))
        {
            if ((!float.IsNaN(angles[0])) && ((!float.IsInfinity(angles[0]))))
            {
                if (angles[0] > 89f)
                {
                    angles[0] = 89f;
                }
                if (angles[0] < -89f)
                {
                    angles[0] = -89f;
                }
                if (angles[1] > 180f)
                {
                    angles[1] = 180f;
                }
                if (angles[1] < -180f)
                {
                    angles[1] = -180f;
                }
                angles[2] = 0f;
                return true;
            }
            return false;
        }
        return false;
    }

    private static bool IsSpottedByMask(int entityID, byte[] mask, uint entityBase)
    {
        return (BitConverter.ToInt32(mask, 0) & (1 << entityID)) != 0;
    }

    private static bool IsTriggerBotKeyActive()
    {
        if ((cheatConfig.TriggerKeybind[iuxehHelper.normalWeaponsConfigIndex] != 0L || cheatConfig.TriggerKeybind[iuxehHelper.specialWeaponConfigIndex] != 0L) && IsKeyPressed(cheatConfig.TriggerKeybind[iuxehHelper.normalWeaponsConfigIndex]) && GetActiveWeaponConfigType() > 0)
        {
            return true;
        }
        return false;
    }

    private static uint GetModuleBaseByPID(uint processPID, string moduleName, ref uint moduleSize)
    {
        IntPtr handle = CreateToolhelp32Snapshot(24u, processPID);
        if (handle == default)
        {
            return 0u;
        }
        MODULEENTRY32 moduleEntry = default;
        moduleEntry.dwSize = (uint)Marshal.SizeOf(moduleEntry);
        if (Module32First(handle, ref moduleEntry) || 1 == 0)
        {
            do
            {
                if (Operators.CompareString(moduleName, moduleEntry.szModule, TextCompare: false) == 0)
                {
                    moduleSize = moduleEntry.modBaseSize;
                    return (uint)(int)moduleEntry.modBaseAddr;
                }
            }
            while (Module32Next(handle, ref moduleEntry) ? true : false);
        }
        return 0u;
    }

    private static void TrigggerBot()
    {
        try
        {
            do
            {
                cheatTriggerHelper.lastShootTime = 0L;
                cheatTriggerHelper.ShootDelayActive = false;
                while (IsTriggerBotKeyActive() && iuxehHelper.cheatIsRunning && IsLocalPlayerAlive())
                {
                    cheatTriggerHelper.crosshairdId = BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_iCrosshairId, 4), 0) - 1;
                    if (cheatTriggerHelper.crosshairdId > -1 || cheatTriggerHelper.ShootDelayActive)
                    {
                        if (cheatTriggerHelper.crosshairdId > iuxehHelper.HighestPlayerIndex)
                        {
                            cheatTriggerHelper.crosshairdId = -1;
                        }
                        cheatTriggerHelper.crosshairTargetPtr = 0u;
                        cheatTriggerHelper.crosshairTargetTeam = 0;
                        if (cheatTriggerHelper.crosshairdId > -1)
                        {
                            cheatTriggerHelper.crosshairTargetPtr = BitConverter.ToUInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, (uint)(cheatOffsets.dwEntityList + cheatTriggerHelper.crosshairdId * 16), 4), 0);
                            cheatTriggerHelper.crosshairTargetTeam = BitConverter.ToInt32(ReadCSGOMemory(cheatTriggerHelper.crosshairTargetPtr, cheatOffsets.m_iTeamNum, 4), 0);
                            if (cheatTriggerHelper.crosshairTargetTeam < 2 && cheatTriggerHelper.crosshairTargetTeam > 3)
                            {
                                cheatTriggerHelper.crosshairdId = -1;
                            }
                            else if ((!cheatConfig.FriendlyFire || 1 == 0) && BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_iTeamNum, 4), 0) == cheatTriggerHelper.crosshairTargetTeam)
                            {
                                cheatTriggerHelper.crosshairdId = -1;
                            }
                        }
                        if (cheatTriggerHelper.crosshairdId <= -1 && iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds >= cheatTriggerHelper.lastShootTime + cheatConfig.TriggerAfterShot[iuxehHelper.normalWeaponsConfigIndex])
                        {
                            cheatTriggerHelper.ShootDelayActive = false;
                        }
                        else
                        {
                            cheatTriggerHelper.triggerPause = cheatConfig.TriggerPause[iuxehHelper.normalWeaponsConfigIndex];
                            cheatTriggerHelper.triggerDelay = cheatConfig.TriggerDelay[iuxehHelper.normalWeaponsConfigIndex];
                            cheatTriggerHelper.forceAttackForWeapon = ((iuxehHelper.specialWeaponConfigIndex == 35) ? cheatOffsets.dwForceAttack2 : cheatOffsets.dwForceAttack);
                            cheatTriggerHelper.targetOutOfDistance = false;
                            if (iuxehHelper.specialWeaponConfigIndex == 35 || iuxehHelper.specialWeaponConfigIndex == 36 || 1 == 0)
                            {
                                cheatTriggerHelper.positionBuffer = ReadCSGOMemory(cheatTriggerHelper.crosshairTargetPtr, cheatOffsets.m_vecOrigin, 12);
                                cheatTriggerHelper.targetPosition = new float[3]
                                {
                                    BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 0),
                                    BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 4),
                                    BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 8)
                                };
                                cheatTriggerHelper.positionBuffer = ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_vecOrigin, 12);
                                cheatTriggerHelper.localplayerPosition = new float[3]
                                {
                                    BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 0),
                                    BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 4),
                                    BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 8)
                                };
                                if (DistanceBetween(cheatTriggerHelper.localplayerPosition, cheatTriggerHelper.targetPosition) < ((iuxehHelper.specialWeaponConfigIndex == 35) ? 65f : 190f))
                                {
                                    cheatTriggerHelper.triggerDelay = cheatConfig.TriggerDelay[iuxehHelper.specialWeaponConfigIndex];
                                    cheatTriggerHelper.triggerPause = cheatConfig.TriggerPause[iuxehHelper.specialWeaponConfigIndex];
                                }
                                else
                                {
                                    cheatTriggerHelper.targetOutOfDistance = true;
                                }
                            }
                            cheatTriggerHelper.forceAttackFlags = BitConverter.ToInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, cheatTriggerHelper.forceAttackForWeapon, 4), 0);
                            if ((cheatTriggerHelper.forceAttackFlags & 3) == 0 && !cheatTriggerHelper.targetOutOfDistance)
                            {
                                if ((!IsCurrentWeaponAutomatic() || cheatTriggerHelper.triggerPause > 0) && iuxehHelper.normalWeaponsConfigIndex != 10)
                                {
                                    Sleep((uint)cheatTriggerHelper.triggerDelay);
                                    WriteCSGOMemory(iuxehHelper.clientBaseAddress, cheatTriggerHelper.forceAttackForWeapon, BitConverter.GetBytes(cheatTriggerHelper.forceAttackFlags | 2));
                                    Sleep((uint)(25 + cheatTriggerHelper.triggerPause));
                                    if (cheatTriggerHelper.crosshairdId > -1)
                                    {
                                        cheatTriggerHelper.ShootDelayActive = true;
                                        cheatTriggerHelper.lastShootTime = iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds;
                                    }
                                    if (cheatTriggerHelper.crosshairdId > -1)
                                    {
                                        cheatTriggerHelper.positionBuffer = ReadCSGOMemory(cheatTriggerHelper.crosshairTargetPtr, cheatOffsets.m_vecOrigin, 12);
                                        cheatTriggerHelper.targetPosition = new float[3]
                                        {
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 0),
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 4),
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 8)
                                        };
                                        cheatTriggerHelper.positionBuffer = ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_vecOrigin, 12);
                                        cheatTriggerHelper.localplayerPosition = new float[3]
                                        {
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 0),
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 4),
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 8)
                                        };
                                        cheatTriggerHelper.distance = DistanceBetween(cheatTriggerHelper.localplayerPosition, cheatTriggerHelper.targetPosition);
                                        while (IsTriggerBotKeyActive() && !((double)GetAimPunchAngle()[0] >= -100.0 / Math.Pow(((!(cheatTriggerHelper.distance > 100f)) ? 100f : ((cheatTriggerHelper.distance > 800f) ? 800f : cheatTriggerHelper.distance)) / 25f, 2.0)))
                                        {
                                            Sleep(1);
                                        }
                                    }
                                }
                                else
                                {
                                    Sleep((uint)cheatTriggerHelper.triggerDelay);
                                    WriteCSGOMemory(iuxehHelper.clientBaseAddress, cheatTriggerHelper.forceAttackForWeapon, BitConverter.GetBytes(cheatTriggerHelper.forceAttackFlags | 1));
                                    cheatTriggerHelper.lastShootSet = false;
                                    if (cheatTriggerHelper.crosshairdId > -1)
                                    {
                                        cheatTriggerHelper.ShootDelayActive = true;
                                        cheatTriggerHelper.lastShootTime = iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds;
                                    }
                                    if (iuxehHelper.normalWeaponsConfigIndex != 10)
                                    {
                                        Sleep(50);
                                    }
                                    else
                                    {
                                        Sleep(200);
                                    }
                                    if (iuxehHelper.normalWeaponsConfigIndex == 10)
                                    {
                                        while (IsTriggerBotKeyActive() && BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_iShotsFired, 4), 0) == 0 && cheatTriggerHelper.crosshairdId == BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_iCrosshairId, 4), 0) - 1)
                                        {
                                            Sleep(1);
                                        }
                                    }
                                    while (IsTriggerBotKeyActive() && iuxehHelper.normalWeaponsConfigIndex != 10)
                                    {
                                        if (ReadCSGOMemory(cheatTriggerHelper.crosshairTargetPtr, cheatOffsets.m_lifeState, 1)[0] != 0 || 1 == 0)
                                        {
                                            if ((!cheatTriggerHelper.lastShootSet || 1 == 0) && cheatTriggerHelper.crosshairdId > -1)
                                            {
                                                cheatTriggerHelper.lastShootTime = iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds;
                                                cheatTriggerHelper.lastShootSet = true;
                                            }
                                            if (iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds > cheatTriggerHelper.lastShootTime + cheatConfig.TriggerAfterShot[iuxehHelper.normalWeaponsConfigIndex])
                                            {
                                                break;
                                            }
                                        }
                                        if (cheatTriggerHelper.crosshairdId != BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_iCrosshairId, 4), 0) - 1 && BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_iShotsFired, 4), 0) < 2)
                                        {
                                            break;
                                        }
                                        Sleep(1);
                                    }
                                    WriteCSGOMemory(iuxehHelper.clientBaseAddress, cheatTriggerHelper.forceAttackForWeapon, BitConverter.GetBytes(cheatTriggerHelper.forceAttackFlags & -2));
                                    Sleep(25);
                                    if (cheatTriggerHelper.crosshairdId > -1 && BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_iShotsFired, 4), 0) < 2)
                                    {
                                        cheatTriggerHelper.positionBuffer = ReadCSGOMemory(cheatTriggerHelper.crosshairTargetPtr, cheatOffsets.m_vecOrigin, 12);
                                        cheatTriggerHelper.targetPosition = new float[3]
                                        {
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 0),
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 4),
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 8)
                                        };
                                        cheatTriggerHelper.positionBuffer = ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_vecOrigin, 12);
                                        cheatTriggerHelper.localplayerPosition = new float[3]
                                        {
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 0),
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 4),
                                            BitConverter.ToSingle(cheatTriggerHelper.positionBuffer, 8)
                                        };
                                        cheatTriggerHelper.distance = DistanceBetween(cheatTriggerHelper.localplayerPosition, cheatTriggerHelper.targetPosition);
                                        while ((IsTriggerBotKeyActive() || 1 == 0) && !((double)GetAimPunchAngle()[0] >= -100.0 / Math.Pow(((!(cheatTriggerHelper.distance > 100f)) ? 100f : ((cheatTriggerHelper.distance > 800f) ? 800f : cheatTriggerHelper.distance)) / 25f, 2.0)))
                                        {
                                            Sleep(1);
                                        }
                                    }
                                }
                            }
                            else if ((cheatTriggerHelper.forceAttackFlags & 1) != 0)
                            {
                                WriteCSGOMemory(iuxehHelper.clientBaseAddress, cheatTriggerHelper.forceAttackForWeapon, BitConverter.GetBytes(cheatTriggerHelper.forceAttackFlags & -2));
                                Sleep(25);
                                break;
                            }
                        }
                    }
                    Sleep(1);
                }
                if ((BitConverter.ToInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, cheatTriggerHelper.forceAttackForWeapon, 4), 0) & 1) != 0)
                {
                    WriteCSGOMemory(iuxehHelper.clientBaseAddress, cheatTriggerHelper.forceAttackForWeapon, BitConverter.GetBytes(cheatTriggerHelper.forceAttackFlags & -2));
                    Sleep(25);
                }
                Sleep(1);
            }
            while (IsTriggerBotKeyActive() && iuxehHelper.cheatIsRunning);
        }
        catch (Exception projectError)
        {
            ProjectData.SetProjectError(projectError);
            ProjectData.ClearProjectError();
        }
        cheatTriggerHelper.TriggerIsrunning = false;
        cheatTriggerHelper.lastShootTime = 0L;
        cheatTriggerHelper.ShootDelayActive = false;
    }

    private static void UpdateThread()
    {
        try
        {
            while (iuxehHelper.modulesLoaded && GetForegroundWindowProcessID() == iuxehHelper.csgoPID && IsInGame() && !iuxehHelper.panicButtonActive)
            {
                if (!iuxehHelper.cheatIsRunning)
                {
                    if (iuxehHelper.mouse_keyboardHookThreadID != 0L)
                    {
                        PostThreadMessageA(iuxehHelper.mouse_keyboardHookThreadID, 18u, 0, default);
                    }
                    while (MouseKeybaordHook_Thread.IsAlive || GlowEspRadarNoFlash_Thread.IsAlive || SoundEsp_Thread.IsAlive)
                    {
                        Sleep(100);
                    }
                    cheatAimbotHelper.AimbotIsrunning = false;
                    cheatTriggerHelper.TriggerIsrunning = false;
                    cheatBunnyHopHelper.BunnyhopActive = false;
                    iuxehHelper.cheatIsRunning = true;
                    TryStartCheatThreads();
                }
                byte[] entityListBuffer = ReadCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwEntityList, 1056);
                int maxPlayerIndex = -1;
                int currendIndex = 0;
                do
                {
                    EntityInfo entity = GetEntityInfo(BitConverter.ToUInt32(entityListBuffer, currendIndex * 16), currendIndex);
                    if (entity.EntityBase != GetLocalPlayer() && entity.EntityBase != 0 && (entity.TeamNum == 2 || entity.TeamNum == 3) && IsTargetable(entity.IsAlly) && currendIndex > maxPlayerIndex)
                    {
                        maxPlayerIndex = currendIndex;
                    }
                    currendIndex++;
                }
                while (currendIndex <= 65);
                iuxehHelper.HighestPlayerIndex = maxPlayerIndex;
                int weaponConfigIndex = GetWeaponConfigIndex();
                iuxehHelper.specialWeaponConfigIndex = ((weaponConfigIndex > 34) ? weaponConfigIndex : 0);
                iuxehHelper.normalWeaponsConfigIndex = ((weaponConfigIndex < 35) ? weaponConfigIndex : 0);
                if (FaceitMode())
                {
                    iuxehHelper.Sensitivity = BitConverter.ToSingle(BitConverter.GetBytes(cheatOffsets.dwSensitivityPtr ^ BitConverter.ToInt32(ReadCSGOMemory(cheatOffsets.dwSensitivityPtr, 44u, 4), 0)), 0);
                    iuxehHelper.floatZoomSensitivity_4 = BitConverter.ToSingle(BitConverter.GetBytes(cheatOffsets.dwZoomSensitivityRatioPtr ^ BitConverter.ToInt32(ReadCSGOMemory(cheatOffsets.dwZoomSensitivityRatioPtr, 44u, 4), 0)), 0);
                    iuxehHelper.pitchClass = BitConverter.ToSingle(BitConverter.GetBytes(cheatOffsets.m_pitchClassPtr ^ BitConverter.ToInt32(ReadCSGOMemory(cheatOffsets.m_pitchClassPtr, 44u, 4), 0)), 0);
                    iuxehHelper.YawPtr = BitConverter.ToSingle(BitConverter.GetBytes(cheatOffsets.dwYawPtr ^ BitConverter.ToInt32(ReadCSGOMemory(cheatOffsets.dwYawPtr, 44u, 4), 0)), 0);
                    iuxehHelper.IsScoped = (ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_bIsScoped, 1)[0] != 0);
                }
                Sleep(100);
            }
            iuxehHelper.cheatIsRunning = false;
        }
        catch (Exception projectError)
        {
            ProjectData.SetProjectError(projectError);
            ProjectData.ClearProjectError();
        }
    }

    private static void InvalidPEHeader()
    {
        IntPtr baseAddress = Process.GetCurrentProcess().MainModule.BaseAddress;
        int e_lfanew = BitConverter.ToInt32(ReadInternalMemory(AddToPointer(baseAddress, 60L), 4), 0);
        int FirstSection = BitConverter.ToInt16(ReadInternalMemory(AddToPointer(baseAddress, e_lfanew + 20), 2), 0) + 24;
        int NumberOfSections = BitConverter.ToInt16(ReadInternalMemory(AddToPointer(baseAddress, e_lfanew + 6), 2), 0);
        WriteToOnlyReadableMemory(AddToPointer(baseAddress, e_lfanew + 6), BitConverter.GetBytes((short)GetRandom(NumberOfSections, 32767)));
        for (int i = 0; i < NumberOfSections; i++)
        {
            int[] SectionDWORDFieldsOffsets = new int[6]
            {
                8,
                16,
                20,
                24,
                28,
                36
            };
            int[] SectionWORDFieldsOffsets = new int[2]
            {
                32,
                34
            };
            for (int j = 0; j < SectionDWORDFieldsOffsets.Length; j++)
            {
                int currentValue = BitConverter.ToInt32(ReadInternalMemory(AddToPointer(baseAddress, e_lfanew + FirstSection + 40 * i + SectionDWORDFieldsOffsets[j]), 4), 0);
                WriteToOnlyReadableMemory(AddToPointer(baseAddress, e_lfanew + FirstSection + 40 * i + SectionDWORDFieldsOffsets[j]), BitConverter.GetBytes(GetRandom(currentValue, int.MaxValue)));
            }
            for (int k = 0; k < SectionWORDFieldsOffsets.Length; k++)
            {
                int currentValue = BitConverter.ToInt16(ReadInternalMemory(AddToPointer(baseAddress, e_lfanew + FirstSection + 40 * i + SectionWORDFieldsOffsets[k]), 2), 0);
                WriteToOnlyReadableMemory(AddToPointer(baseAddress, e_lfanew + FirstSection + 40 * i + SectionWORDFieldsOffsets[k]), BitConverter.GetBytes((short)GetRandom(currentValue, 32767)));
            }
            byte[] sectionName = new byte[8];
            for (int l = 0; l < sectionName.Length; l++)
                sectionName[l] = (byte)GetRandom(0, 255);

            WriteToOnlyReadableMemory(AddToPointer(baseAddress, e_lfanew + FirstSection + 40 * i), sectionName);
        }
    }

    private static void InitPatternScanner(uint clientBase, uint clientSize, uint engineBase, uint engineSize)
    {
        byte[] clientBuffer = ReadCSGOMemory(clientBase, 0u, (int)clientSize);
        cheatOffsets.dwForceAttack = FindPatternInMemory(clientBuffer, new byte[19]
        {
            137,
            13,
            0,
            0,
            0,
            0,
            139,
            13,
            0,
            0,
            0,
            0,
            139,
            242,
            139,
            193,
            131,
            206,
            4
        }, new byte[19]
        {
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        }, clientBase, 2u, read: true, relative: true, 0u);
        cheatOffsets.dwForceAttack = FindPatternInMemory(clientBuffer, new byte[19]
        {
            137,
            13,
            0,
            0,
            0,
            0,
            139,
            13,
            0,
            0,
            0,
            0,
            139,
            242,
            139,
            193,
            131,
            206,
            4
        }, new byte[19]
        {
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        }, clientBase, 2u, read: true, relative: true, 0u);
        cheatOffsets.dwForceAttack2 = FindPatternInMemory(clientBuffer, new byte[19]
        {
            137,
            13,
            0,
            0,
            0,
            0,
            139,
            13,
            0,
            0,
            0,
            0,
            139,
            242,
            139,
            193,
            131,
            206,
            4
        }, new byte[19]
        {
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        }, clientBase, 2u, read: true, relative: true, 12u);
        cheatOffsets.dwForceJump = FindPatternInMemory(clientBuffer, new byte[19]
        {
            137,
            13,
            0,
            0,
            0,
            0,
            139,
            13,
            0,
            0,
            0,
            0,
            139,
            242,
            139,
            193,
            131,
            206,
            8
        }, new byte[19]
        {
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        }, clientBase, 2u, read: true, relative: true, 0u);
        cheatOffsets.dwEntityList = FindPatternInMemory(clientBuffer, new byte[16]
        {
            187,
            0,
            0,
            0,
            0,
            131,
            255,
            1,
            15,
            140,
            0,
            0,
            0,
            0,
            59,
            248
        }, new byte[16]
        {
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0
        }, clientBase, 1u, read: true, relative: true, 0u);
        cheatOffsets.dwPlayerResource = FindPatternInMemory(clientBuffer, new byte[16]
        {
            139,
            61,
            0,
            0,
            0,
            0,
            133,
            255,
            15,
            132,
            0,
            0,
            0,
            0,
            129,
            199
        }, new byte[16]
        {
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0
        }, clientBase, 2u, read: true, relative: true, 0u);
        cheatOffsets.dwLocalPlayer = FindPatternInMemory(clientBuffer, new byte[22]
        {
            141,
            52,
            133,
            0,
            0,
            0,
            0,
            137,
            21,
            0,
            0,
            0,
            0,
            139,
            65,
            8,
            139,
            72,
            4,
            131,
            249,
            255
        }, new byte[22]
        {
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        }, clientBase, 3u, read: true, relative: true, 4u);
        cheatOffsets.dwMouseEnable = (uint)(iuxehHelper.clientBaseAddress + 13912048L);
        cheatOffsets.dwSensitivityPtr = (uint)(iuxehHelper.clientBaseAddress + 13911648L);
        cheatOffsets.dwYawPtr = (uint)(iuxehHelper.clientBaseAddress + 13911120L);
        cheatOffsets.dwZoomSensitivityRatioPtr = (uint)(iuxehHelper.clientBaseAddress + 13932184L);
        cheatOffsets.m_pitchClassPtr = (uint)(iuxehHelper.clientBaseAddress + 85492888L);
        byte[] engineBuffer = ReadCSGOMemory(engineBase, 0u, (int)engineSize);
        cheatOffsets.dwClientState_ViewAngles = FindPatternInMemory(engineBuffer, new byte[13]
        {
            243,
            15,
            17,
            128,
            0,
            0,
            0,
            0,
            217,
            70,
            4,
            217,
            5
        }, new byte[13]
        {
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0
        }, engineBase, 4u, read: true, relative: false, 0u);
        cheatOffsets.dwClientState = FindPatternInMemory(engineBuffer, new byte[15]
        {
            161,
            0,
            0,
            0,
            0,
            51,
            210,
            106,
            0,
            106,
            0,
            51,
            201,
            137,
            176
        }, new byte[15]
        {
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        }, engineBase, 1u, read: true, relative: true, 0u);
        cheatOffsets.dwLocalPosition = FindPatternInMemory(engineBuffer, new byte[31]
        {
            232,
            0,
            0,
            0,
            0,
            139,
            240,
            133,
            246,
            15,
            132,
            0,
            0,
            0,
            0,
            104,
            0,
            0,
            0,
            0,
            106,
            0,
            139,
            206,
            232,
            0,
            0,
            0,
            0,
            139,
            13
        }, new byte[31]
        {
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            0,
            0
        }, engineBase, 16u, read: true, relative: true, 4u);
        cheatOffsets.dwGlobalVars = FindPatternInMemory(engineBuffer, new byte[15]
        {
            104,
            0,
            0,
            0,
            0,
            104,
            0,
            0,
            0,
            0,
            255,
            80,
            8,
            133,
            192
        }, new byte[15]
        {
            0,
            1,
            1,
            1,
            1,
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            0
        }, engineBase, 1u, read: true, relative: true, 0u);
        cheatOffsets.dwGlowObjectManager = FindPatternInMemory(clientBuffer, new byte[14]
        {
            161,
            0,
            0,
            0,
            0,
            168,
            1,
            117,
            75,
            15,
            87,
            192,
            199,
            5
        }, new byte[16]
        {
            0,
            1,
            1,
            1,
            1,
            0,
            0,
            0,
            0,
            1,
            1,
            1,
            1,
            1,
            1,
            1
        }, clientBase, 1u, read: true, relative: true, 4u);
    }

    private static IntPtr OpenProcess(uint DesiredAccess, uint dwProcessId)
    {
        CLIENT_ID clientID = default;
        clientID.UniqueProcess = new IntPtr(dwProcessId);
        clientID.UniqueThread = default;
        OBJECT_ATTRIBUTES objectAttributes = default;
        objectAttributes.Length = Marshal.SizeOf(objectAttributes);
        IntPtr processHandle = default;
        NtOpenProcess(ref processHandle, DesiredAccess, ref objectAttributes, ref clientID);
        return processHandle;
    }

    private static float[] GetAimPunchAngle()
    {
        byte[] aimpunchBuffer = ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_aimPunchAngle, 12);
        return new float[3]
        {
            BitConverter.ToSingle(aimpunchBuffer, 0),
            BitConverter.ToSingle(aimpunchBuffer, 4),
            BitConverter.ToSingle(aimpunchBuffer, 8)
        };
    }

    private static void WriteToOnlyReadableMemory(IntPtr pointer, byte[] buffer)
    {
        uint OldProtect = 0u;
        VirtualProtect(pointer, (uint)buffer.Length, 64u, ref OldProtect); //PAGE_EXECUTE_READWRITE
        for (int i = 0; i < buffer.Length; i++)
            Marshal.WriteByte(new IntPtr(pointer.ToInt64() + i), buffer[i]);
        VirtualProtect(pointer, (uint)buffer.Length, OldProtect, ref OldProtect);
    }

    private static bool IsInGame()
    {
        if (iuxehHelper.clientBaseAddress != 0 && iuxehHelper.engineBaseAddress != 0 && GetLocalPlayer() != 0)
        {
            iuxehHelper.clientState = BitConverter.ToUInt32(ReadCSGOMemory(iuxehHelper.engineBaseAddress, cheatOffsets.dwClientState, 4), 0);
            if (BitConverter.ToInt32(ReadCSGOMemory(iuxehHelper.clientState, cheatOffsets.dwClientState_State, 4), 0) == 6)
            {
                return true;
            }
        }
        return false;
    }

    private static void SetKeyState(uint vkey, bool val)
    {
        lock (iuxehHelper.keyState)
        {
            iuxehHelper.keyState[vkey] = val;
        }
    }

    private static int HookCallbackMouse(int nCode, int wParam, ref MSLLHOOKSTRUCT mouse)
    {
        if (nCode < 0)
        {
            return CallNextHookEx_mouse(iuxehHelper.mouseHookHandle, nCode, wParam, mouse);
        }
        switch (wParam)
        {
            case 512:
                if (mouse.flags > 0L && FaceitMode())
                {
                    return 0;
                }
                break;
            case 513:
                SetKeyStateWrapper(1u, val: true);
                OnKeyPressed(1u);
                break;
            case 514:
                SetKeyStateWrapper(1u, val: false);
                break;
            case 516:
                SetKeyStateWrapper(2u, val: true);
                OnKeyPressed(2u);
                break;
            case 517:
                SetKeyStateWrapper(2u, val: false);
                break;
            case 519:
                SetKeyStateWrapper(4u, val: true);
                OnKeyPressed(4u);
                break;
            case 520:
                SetKeyStateWrapper(4u, val: false);
                break;
            case 523:
                switch (mouse.mouseData >> 16)
                {
                    case 1u:
                        SetKeyStateWrapper(5u, val: true);
                        OnKeyPressed(5u);
                        break;
                    case 2u:
                        SetKeyStateWrapper(6u, val: true);
                        OnKeyPressed(6u);
                        break;
                }
                break;
            case 524:
                switch (mouse.mouseData >> 16)
                {
                    case 1u:
                        SetKeyStateWrapper(5u, val: false);
                        break;
                    case 2u:
                        SetKeyStateWrapper(6u, val: false);
                        break;
                }
                break;
        }
        return CallNextHookEx_mouse(iuxehHelper.mouseHookHandle, nCode, wParam, mouse);
    }

    [STAThread]
    public static void Main()
    {
        InvalidPEHeader();
        //FillSettingsFromMemory(); my secure auth, don't care about it
        iuxehHelper.cheatStartStopWatch.Start();
        iuxehHelper.startTime = iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds;
        Thread MainLoop_Thread = new Thread(MainLoop);
        MainLoop_Thread.Start();
    }

    private static float[] GetViewAngles()
    {
        if (!FaceitMode())
        {
            byte[] bufferAngles = ReadCSGOMemory(iuxehHelper.clientState, cheatOffsets.dwClientState_ViewAngles, 12);
            float[] viewAngles = new float[3]
            {
                BitConverter.ToSingle(bufferAngles, 0),
                BitConverter.ToSingle(bufferAngles, 4),
                BitConverter.ToSingle(bufferAngles, 8)
            };
            NormalizeAngles(ref viewAngles);
            return viewAngles;
        }
        byte[] buffer = ReadCSGOMemory(GetLocalPlayer(), 12760u, 12); //m_thirdPersonViewAngles
        float[] viewAnglesThird = new float[3]
        {
            BitConverter.ToSingle(buffer, 0),
            BitConverter.ToSingle(buffer, 4),
            BitConverter.ToSingle(buffer, 8)
        };
        NormalizeAngles(ref viewAnglesThird);
        return viewAnglesThird;
    }

    private static bool LoadConfigFromArray(string[] configArray)
    {
        if (configArray.Length == 699)
        {
            cheatConfig.BhopKey = TryParseUintFromString(configArray[0]);
            cheatConfig.MasterBreak = TryParseIntegerFromString(configArray[1]);
            cheatConfig.FaceitMode = (TryParseIntegerFromString(configArray[2]) != 0);
            cheatConfig.FriendlyFire = (TryParseIntegerFromString(configArray[3]) != 0);
            cheatConfig.GlowESPKeybind = TryParseUintFromString(configArray[4]);
            cheatConfig.RadarEnabled = (TryParseIntegerFromString(configArray[5]) != 0);
            cheatConfig.NoFlashAmount = TryParseFloatFromString(configArray[6]);
            cheatConfig.PanicKey = TryParseUintFromString(configArray[7]);
            cheatConfig.SoundESPEnabled = (TryParseIntegerFromString(configArray[8]) != 0);
            cheatConfig.SoundESPPitch = TryParseUintFromString(configArray[9]);
            cheatConfig.SoundESPMaxRange = TryParseFloatFromString(configArray[10]);
            int currentIndex = 1;
            do
            {
                if (currentIndex >= 35)
                {
                    if (currentIndex > 34)
                    {
                        int specialIndex = 0;
                        do
                        {
                            cheatConfig.TriggerKeybind[currentIndex] = TryParseUintFromString(configArray[691 + specialIndex * 4 + 1]);
                            cheatConfig.TriggerDelay[currentIndex] = TryParseIntegerFromString(configArray[691 + specialIndex * 4 + 2]);
                            cheatConfig.TriggerPause[currentIndex] = TryParseIntegerFromString(configArray[691 + specialIndex * 4 + 3]);
                            specialIndex++;
                        }
                        while (specialIndex <= 1);
                    }
                }
                else
                {
                    int weaponIndex = 11 + (currentIndex - 1) * 20;
                    cheatConfig.AimbotKey[currentIndex] = TryParseUintFromString(configArray[weaponIndex + 1]);
                    cheatConfig.AimbotRotationAssist[currentIndex] = (TryParseIntegerFromString(configArray[weaponIndex + 2]) != 0);
                    cheatConfig.AimbotStick[currentIndex] = TryParseIntegerFromString(configArray[weaponIndex + 3]);
                    cheatConfig.AimbotFov[currentIndex] = TryParseFloatFromString(configArray[weaponIndex + 4]);
                    cheatConfig.AimbotBoneSeletion[currentIndex] = TryParseIntegerFromString(configArray[weaponIndex + 5]);
                    cheatConfig.AimbotBone[currentIndex, 3] = TryParseIntegerFromString(configArray[weaponIndex + 6]);
                    cheatConfig.AimbotBone[currentIndex, 2] = TryParseIntegerFromString(configArray[weaponIndex + 7]);
                    cheatConfig.AimbotBone[currentIndex, 1] = TryParseIntegerFromString(configArray[weaponIndex + 8]);
                    cheatConfig.AimbotBone[currentIndex, 0] = TryParseIntegerFromString(configArray[weaponIndex + 9]);
                    cheatConfig.AimbotSmooth[currentIndex] = TryParseFloatFromString(configArray[weaponIndex + 10]);
                    cheatConfig.AimDelay[currentIndex] = TryParseIntegerFromString(configArray[weaponIndex + 11]);
                    cheatConfig.AimTime[currentIndex] = TryParseIntegerFromString(configArray[weaponIndex + 12]);
                    cheatConfig.AimbotThroughWalls[currentIndex] = (TryParseIntegerFromString(configArray[weaponIndex + 13]) != 0);
                    cheatConfig.TriggerKeybind[currentIndex] = TryParseUintFromString(configArray[weaponIndex + 14]);
                    cheatConfig.TriggerDelay[currentIndex] = TryParseIntegerFromString(configArray[weaponIndex + 15]);
                    cheatConfig.TriggerPause[currentIndex] = TryParseIntegerFromString(configArray[weaponIndex + 16]);
                    cheatConfig.TriggerAfterShot[currentIndex] = TryParseIntegerFromString(configArray[weaponIndex + 17]);
                    cheatConfig.HorizontalRecoil[currentIndex] = TryParseFloatFromString(configArray[weaponIndex + 18]);
                    cheatConfig.VerticalRecoil[currentIndex] = TryParseFloatFromString(configArray[weaponIndex + 19]);
                }
                currentIndex++;
            }
            while (currentIndex <= 36);
            return true;
        }
        return false;
    }

    private static EntityInfo GetEntityInfo(uint entityBase, int entityIndex)
    {
        byte[] entityBuffer = ReadCSGOMemory(entityBase, cheatOffsets.m_bDormant, (int)(cheatOffsets.m_lifeState - cheatOffsets.m_bDormant + 1L));
        EntityInfo result = default;
        result.EntityBase = entityBase;
        result.TeamNum = BitConverter.ToInt16(entityBuffer, (int)(cheatOffsets.m_iTeamNum - cheatOffsets.m_bDormant));
        result.IsAlly = ((GetLocalPlayerTeamNum() == result.TeamNum));
        result.LifeState = entityBuffer[cheatOffsets.m_lifeState - cheatOffsets.m_bDormant];
        result.Index = entityIndex;
        result.IsDormant = BitConverter.ToBoolean(entityBuffer, 0);
        result.vecOrigin = new float[3]
        {
            BitConverter.ToSingle(entityBuffer, (int)(cheatOffsets.m_vecOrigin - cheatOffsets.m_bDormant)),
            BitConverter.ToSingle(entityBuffer, (int)(cheatOffsets.m_vecOrigin - cheatOffsets.m_bDormant + 4)),
            BitConverter.ToSingle(entityBuffer, (int)(cheatOffsets.m_vecOrigin - cheatOffsets.m_bDormant + 8))
        };
        result.vecOrigin = new float[3]
        {
            BitConverter.ToSingle(entityBuffer, (int)(cheatOffsets.m_vecOrigin - cheatOffsets.m_bDormant)),
            BitConverter.ToSingle(entityBuffer, (int)(cheatOffsets.m_vecOrigin - cheatOffsets.m_bDormant + 4)),
            BitConverter.ToSingle(entityBuffer, (int)(cheatOffsets.m_vecOrigin - cheatOffsets.m_bDormant + 8))
        };
        return result;
    }

    private static void SmoothAngles(float smooth, float[] ViewAngles, ref float[] aimViewAngles)
    {
        if (smooth > 0f)
        {
            aimViewAngles[0] = aimViewAngles[0] - ViewAngles[0];
            aimViewAngles[1] = aimViewAngles[1] - ViewAngles[1];
            NormalizeAngles(ref aimViewAngles);
            aimViewAngles[0] = ViewAngles[0] + aimViewAngles[0] / smooth;
            aimViewAngles[1] = ViewAngles[1] + aimViewAngles[1] / smooth;
            NormalizeAngles(ref aimViewAngles);
        }
    }

    public static void AimbotRcs()
    {
        try
        {
            cheatAimbotHelper.aimbotSmooth = ((cheatConfig.AimbotSmooth[iuxehHelper.normalWeaponsConfigIndex] >= 1f) ? (cheatConfig.AimbotSmooth[iuxehHelper.normalWeaponsConfigIndex] + 1f) : 0f) / 2f;
            cheatAimbotHelper.aimbotKeyPressed = IsKeyPressed(cheatConfig.AimbotKey[iuxehHelper.normalWeaponsConfigIndex]);
            cheatAimbotHelper.isTriggerKeyPressed = IsTriggerKeyPressed();
            cheatAimbotHelper.aimbotFov = cheatConfig.AimbotFov[iuxehHelper.normalWeaponsConfigIndex];
            int pitchMove = default;
            int yawMove = default;
            do
            {
                cheatAimbotHelper.unsedValue2 = false;
                if (IsLocalPlayerAlive() && GetActiveWeaponConfigType() == 1)
                {
                    if (cheatAimbotHelper.IsAimDelaySet)
                    {
                        if (cheatConfig.AimDelay[iuxehHelper.normalWeaponsConfigIndex] > 0L)
                        {
                            cheatAimbotHelper.aimDelayValue = iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds + cheatConfig.AimDelay[iuxehHelper.normalWeaponsConfigIndex];
                        }
                        cheatAimbotHelper.IsAimDelaySet = false;
                    }
                    cheatAimbotHelper.aimbotStick = (cheatConfig.AimbotStick[iuxehHelper.normalWeaponsConfigIndex] == 1);
                    cheatAimbotHelper.aimThroughWalls = cheatConfig.AimbotThroughWalls[iuxehHelper.normalWeaponsConfigIndex];
                    cheatAimbotHelper.fovStickValue = 11f;
                    cheatAimbotHelper.aimbotTargetfound = false;
                    cheatAimbotHelper.viewAngles = new float[3]
                    {
                        0f,
                        0f,
                        0f
                    };
                    if (((cheatAimbotHelper.aimbotKeyPressed) && (cheatAimbotHelper.aimDelayValue <= 0L || cheatAimbotHelper.aimDelayValue < iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds)) || (((cheatAimbotHelper.aimStartTime > 0L && cheatAimbotHelper.aimStartTime >= iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds)) && (cheatAimbotHelper.aimDelayValue <= 0L || cheatAimbotHelper.aimDelayValue < iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds)))
                    {
                        cheatAimbotHelper.aimDelayValue = 0L;
                        float[] targetAimAngles = new float[3];
                        float targetFov = float.MaxValue;
                        cheatAimbotHelper.viewAngles = GetViewAngles();
                        if (FoundAimBotTarget(ref cheatAimbotHelper.viewAngles, ref cheatAimbotHelper.lastAimPunch, ref targetFov, ref targetAimAngles, ref cheatAimbotHelper.selectedBone, ref cheatAimbotHelper.targetBase, ref cheatAimbotHelper.lastboneIndex, cheatAimbotHelper.stickTarget, cheatAimbotHelper.aimbotFov, cheatAimbotHelper.aimThroughWalls))
                        {
                            cheatAimbotHelper.aimbotTargetfound = true;
                            if (cheatAimbotHelper.targetBase != cheatAimbotHelper.lastTargetBase)
                            {
                                if (!cheatAimbotHelper.aimbotStick)
                                {
                                    cheatAimbotHelper.stickTarget = false;
                                    cheatAimbotHelper.unusedValue = 0L;
                                }
                                if (cheatAimbotHelper.lastTargetBase != 0)
                                {
                                    cheatAimbotHelper.aimDelayValueSecond = iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds + 250L;
                                }
                                cheatAimbotHelper.lastTargetBase = cheatAimbotHelper.targetBase;
                            }
                            if (targetFov < cheatAimbotHelper.fovStickValue && (!cheatAimbotHelper.aimbotStick))
                            {
                                cheatAimbotHelper.stickTarget = true;
                            }
                            if ((cheatAimbotHelper.aimDelayValueSecond == 0L || cheatAimbotHelper.aimDelayValueSecond < iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds) && (!cheatAimbotHelper.stickTarget))
                            {
                                SmoothAngles(cheatAimbotHelper.aimbotSmooth, cheatAimbotHelper.viewAngles, ref targetAimAngles);
                                if (SetViewAngles(targetAimAngles, ref cheatAimbotHelper.viewAngles, unUsed: false, ref pitchMove, ref yawMove))
                                {
                                    cheatAimbotHelper.lastTargetBase = cheatAimbotHelper.targetBase;
                                }
                            }
                            if (cheatAimbotHelper.aimbotKeyPressed)
                            {
                                cheatAimbotHelper.aimStartTime = iuxehHelper.cheatStartStopWatch.ElapsedMilliseconds + cheatConfig.AimTime[iuxehHelper.normalWeaponsConfigIndex];
                            }
                        }
                    }
                    int ShotsFired = BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_iShotsFired, 4), 0);
                    float[] aimPunch = GetAimPunchAngle();
                    if (ShotsFired > ((IsCurrentWeaponAutomatic()) ? 1 : 0) || ((ShotsFired == 0 && aimPunch[0] != 0f)))
                    {
                        cheatAimbotHelper.lastAimPunch = new float[2]
                        {
                            aimPunch[0],
                            aimPunch[1]
                        };
                    }
                    cheatAimbotHelper.viewAngles = GetViewAngles();
                    RecoilControlSystem(aimPunch, ref cheatAimbotHelper.lastAimPunchValues, cheatAimbotHelper.viewAngles, ShotsFired, cheatAimbotHelper.aimbotTargetfound, ref cheatAimbotHelper.lastAimPunch);
                }
                cheatAimbotHelper.aimbotKeyPressed = IsKeyPressed(cheatConfig.AimbotKey[iuxehHelper.normalWeaponsConfigIndex]);
                cheatAimbotHelper.isTriggerKeyPressed = IsTriggerKeyPressed();
                if ((!cheatAimbotHelper.aimbotKeyPressed) && (!IsKeyPressed(1u)) && (!cheatAimbotHelper.isTriggerKeyPressed))
                {
                    cheatAimbotHelper.stickTarget = false;
                    if (cheatAimbotHelper.lastAimPunchValues[0] == 0f && cheatAimbotHelper.lastAimPunchValues[1] == 0f)
                    {
                        cheatAimbotHelper.lastAimPunchIsZero = true;
                    }
                }
                if (!cheatAimbotHelper.lastAimPunchIsZero)
                {
                    Sleep(1);
                }
            }
            while ((!cheatAimbotHelper.lastAimPunchIsZero) && (iuxehHelper.cheatIsRunning));
        }
        catch (Exception projectError)
        {
            ProjectData.SetProjectError(projectError);
            ProjectData.ClearProjectError();
        }
        cheatAimbotHelper.lastAimPunchIsZero = false;
        cheatAimbotHelper.lastAimPunchValues = new float[4]
        {
            0f,
            0f,
            0f,
            0f
        };
        cheatAimbotHelper.lastAimPunch = new float[2]
        {
            0f,
            0f
        };
        cheatAimbotHelper.lastboneIndex = 0;
        cheatAimbotHelper.aimDelayValueSecond = 0L;
        cheatAimbotHelper.IsAimDelaySet = true;
        cheatAimbotHelper.aimDelayValue = 0L;
        cheatAimbotHelper.aimStartTime = 0L;
        cheatAimbotHelper.stickTarget = false;
        cheatAimbotHelper.lastTargetBase = 0u;
        cheatAimbotHelper.AimbotIsrunning = false;
    }

    private static void SoundESPBeep(int sleepTime)
    {
        Beep(cheatConfig.SoundESPPitch, 140u);
        Sleep((uint)(sleepTime - 140));
    }

    private static void BunnyHop()
    {
        try
        {
            do
            {
                if (IsLocalPlayerAlive())
                {
                    int JumpState = 0;
                    cheatBunnyHopHelper.forceJump = BitConverter.ToInt32(ReadCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwForceJump, 4), 0);
                    cheatBunnyHopHelper.flags = BitConverter.ToInt32(ReadCSGOMemory(GetLocalPlayer(), cheatOffsets.m_fFlags, 4), 0);
                    if (((cheatBunnyHopHelper.flags & 1) != 0) && ((cheatBunnyHopHelper.forceJump & 1) == 0))
                    {
                        WriteCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwForceJump, BitConverter.GetBytes(cheatBunnyHopHelper.forceJump | 1));
                    }
                    else if (((cheatBunnyHopHelper.flags & 1) == 0) && ((cheatBunnyHopHelper.forceJump & 1) != 0))
                    {
                        Sleep(50);
                        WriteCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwForceJump, BitConverter.GetBytes(cheatBunnyHopHelper.forceJump & -2));
                        if (JumpState == 2)
                        {
                            Sleep(50);
                            WriteCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwForceJump, BitConverter.GetBytes(cheatBunnyHopHelper.forceJump | 1));
                            JumpState = -1;
                        }
                        JumpState++;
                    }
                }
                Sleep(1);
            }
            while (IsKeyPressed(cheatConfig.BhopKey) && iuxehHelper.cheatIsRunning);
        }
        catch (Exception projectError)
        {
            ProjectData.SetProjectError(projectError);
            ProjectData.ClearProjectError();
        }
        cheatBunnyHopHelper.BunnyhopActive = false;
    }

    private static bool IsTargetable(bool isAlly)
    {
        if (isAlly && !cheatConfig.FriendlyFire)
        {
            return false;
        }
        return true;
    }

    private static uint FindPatternInMemory(byte[] buffer, byte[] pattern, byte[] mask, uint BaseAddress, uint offset, bool read, bool relative, uint addionalOffset)
    {
        for (uint i = 0u; i < buffer.Length - pattern.Length; i++)
        {
            if (buffer[i] != pattern[0])
            {
                continue;
            }
            int num2 = pattern.Length - 1;
            int num3 = 1;
            while (true)
            {
                if (num3 <= num2)
                {
                    if (mask[num3] != 1 && buffer[(int)(i + num3)] != pattern[num3])
                    {
                        break;
                    }
                    num3++;
                    continue;
                }
                if (read)
                {
                    i = BitConverter.ToUInt32(buffer, (int)(i + offset));
                }
                if ((!read) && (!relative))
                {
                    i = BaseAddress + i;
                }
                if ((read) && (relative))
                {
                    i -= BaseAddress;
                }
                return i + addionalOffset;
            }
        }
        return 0u;
    }

    private static IntPtr AddToPointer(IntPtr pointer, long offset)
    {
        return new IntPtr((long)pointer + offset);
    }

    private static bool CalcAimAngles(ref float[] aimViewAngles, ref float[] sourcePosition, ref float[] targetPosition, ref float[] lastAimPunch, ref float[] viewAngless, ref float aimAnglesFov, ref float distance)
    {
        float[] calcViewAngles = new float[3];
        CalcAng(ref sourcePosition, ref targetPosition, ref lastAimPunch, ref calcViewAngles);
        float fov = GetFov(ref viewAngless, ref calcViewAngles, ref distance);
        if (fov < aimAnglesFov)
        {
            aimAnglesFov = fov;
            NormalizeAngles(ref calcViewAngles);
            aimViewAngles = calcViewAngles;
            return true;
        }
        return false;
    }

    private static List<EntityInfo> GetValidTargets()
    {
        List<EntityInfo> list = new List<EntityInfo>();
        if (iuxehHelper.HighestPlayerIndex > -1)
        {
            byte[] entityListBuffer = ReadCSGOMemory(iuxehHelper.clientBaseAddress, cheatOffsets.dwEntityList, 16 * (iuxehHelper.HighestPlayerIndex + 1));
            for (int i = 0; i <= iuxehHelper.HighestPlayerIndex; i++)
            {
                EntityInfo item = GetEntityInfo(BitConverter.ToUInt32(entityListBuffer, i * 16), i);
                if (item.EntityBase != GetLocalPlayer() && item.EntityBase != 0 && !item.IsDormant && (item.TeamNum == 3 || item.TeamNum == 2) && IsTargetable(item.IsAlly) && item.LifeState == 0)
                {
                    list.Add(item);
                }
            }
        }
        return list;
    }

    private static uint GetModuleBaseByHandle(IntPtr handle, string moduleName, ref uint regionSize)
    {
        try
        {
            MEMORY_BASIC_INFORMATION memoryInformation = default;
            IntPtr currentAddress = default;
            IntPtr moduleBaseAddress = default;
            regionSize = 0u;
            IntPtr value = default;
            while (VirtualQueryEx(handle, currentAddress, ref memoryInformation, (uint)Marshal.SizeOf(memoryInformation)) != 0L && (long)currentAddress < 2147483647L)
            {
                if (memoryInformation.State == 4096L)
                {
                    try
                    {
                        StringBuilder stringBuilder = new StringBuilder(256);
                        K32GetMappedFileNameA(handle, memoryInformation.BaseAddress, stringBuilder, 256u);
                        if (Operators.CompareString(Path.GetFileName(stringBuilder.ToString()).ToLower(), moduleName, TextCompare: false) == 0)
                        {
                            if (moduleBaseAddress == value)
                            {
                                moduleBaseAddress = memoryInformation.BaseAddress;
                            }
                            regionSize += (uint)(int)memoryInformation.RegionSize;
                        }
                        else if (moduleBaseAddress != value)
                        {
                            return (uint)(int)moduleBaseAddress;
                        }
                    }
                    catch (Exception projectError)
                    {
                        ProjectData.SetProjectError(projectError);
                        ProjectData.ClearProjectError();
                    }
                }
                currentAddress = (IntPtr)((long)(int)memoryInformation.BaseAddress + (long)(int)memoryInformation.RegionSize);
            }
        }
        catch (Exception projectError2)
        {
            ProjectData.SetProjectError(projectError2);
            ProjectData.ClearProjectError();
        }
        return 0u;
    }

    private static void CalcAng(ref float[] sourcePosition, ref float[] targetPosition, ref float[] lastAimPunch, ref float[] aimViewAngles)
    {
        float[] delta = new float[3]
        {
            targetPosition[0] - sourcePosition[0],
            targetPosition[1] - sourcePosition[1],
            sourcePosition[2] - targetPosition[2]
        };
        aimViewAngles = new float[3]
        {
            57.29578f * (float)Math.Atan(delta[2] / (float)Math.Pow(Math.Pow(delta[0], 2.0) + Math.Pow(delta[1], 2.0), 0.5)),
            57.29578f * (float)Math.Atan2(delta[1], delta[0]),
            0f
        };
        NormalizeAngles(ref aimViewAngles);
        aimViewAngles = new float[3]
        {
            aimViewAngles[0] - lastAimPunch[0] * 2f,
            aimViewAngles[1] - lastAimPunch[1] * 2f,
            aimViewAngles[2]
        };
        NormalizeAngles(ref aimViewAngles);
    }

    private static bool SetViewAngles(float[] viewangles, ref float[] currentViewAngles, bool unUsed, ref int pitch_move, ref int yaw_move)
    {
        if ((!FaceitMode()) && (ClampAngles(ref viewangles)))
        {
            if (viewangles[0] != currentViewAngles[0] && viewangles[1] != currentViewAngles[1])
            {
                byte[] viewanglesCopy = new byte[8];
                BitConverter.GetBytes(viewangles[0]).CopyTo(viewanglesCopy, 0);
                BitConverter.GetBytes(viewangles[1]).CopyTo(viewanglesCopy, 4);
                WriteCSGOMemory(iuxehHelper.clientState, cheatOffsets.dwClientState_ViewAngles, viewanglesCopy);
                currentViewAngles = new float[3]
                {
                    viewangles[0],
                    viewangles[1],
                    currentViewAngles[2]
                };
                return true;
            }
            if (viewangles[1] != currentViewAngles[1])
            {
                byte[] viewanglesCopy = new byte[4];
                BitConverter.GetBytes(viewangles[1]).CopyTo(viewanglesCopy, 0);
                WriteCSGOMemory(iuxehHelper.clientState, (uint)(cheatOffsets.dwClientState_ViewAngles + 4L), viewanglesCopy);
                currentViewAngles = new float[3]
                {
                    currentViewAngles[0],
                    viewangles[1],
                    currentViewAngles[2]
                };
                return true;
            }
            if (viewangles[0] != currentViewAngles[0])
            {
                byte[] viewanglesCopy = new byte[4];
                BitConverter.GetBytes(viewangles[0]).CopyTo(viewanglesCopy, 0);
                WriteCSGOMemory(iuxehHelper.clientState, cheatOffsets.dwClientState_ViewAngles, viewanglesCopy);
                currentViewAngles = new float[3]
                {
                    viewangles[0],
                    currentViewAngles[1],
                    currentViewAngles[2]
                };
                return true;
            }
        }
        else if ((FaceitMode()) && viewangles[0] > -89f && viewangles[0] < 89f && viewangles[1] > -180f && viewangles[1] < 180f)
        {
            float[] delta = viewangles;
            delta[0] = viewangles[0] - currentViewAngles[0];
            float[] delta2 = viewangles;
            delta2[1] = viewangles[1] - currentViewAngles[1];
            if (viewangles[0] != 0f && viewangles[1] != 0f)
            {
                NormalizeAngles(ref viewangles);
                float sens = GetRealSensitivity();
                pitch_move = (int)Math.Round((0f - viewangles[1]) / (iuxehHelper.pitchClass * sens));
                yaw_move = (int)Math.Round(viewangles[0] / (iuxehHelper.YawPtr * sens));
                if (pitch_move != 0 || yaw_move != 0)
                {
                    MouseMove(pitch_move, yaw_move);
                    currentViewAngles[0] = (float)yaw_move * (iuxehHelper.YawPtr * sens) + currentViewAngles[0];
                    currentViewAngles[1] = (float)(-pitch_move) * (iuxehHelper.pitchClass * sens) + currentViewAngles[1];
                    NormalizeAngles(ref currentViewAngles);
                    Sleep(1);
                    return true;
                }
            }
        }
        return false;
    }

    private static void FillSettingsFromMemory()
    {
        IntPtr imageEndPointer = AddToPointer(Process.GetCurrentProcess().MainModule.BaseAddress, Process.GetCurrentProcess().MainModule.ModuleMemorySize - 8);
        IntPtr pointerToPage = new IntPtr(BitConverter.ToInt64(ReadInternalMemory(imageEndPointer, 8), 0));
        IntPtr configData = AddToPointer(pointerToPage, 200L);
        byte[] configGlobalBuffer = ReadInternalMemory(configData, 52);
        int loaderPID = BitConverter.ToInt32(configGlobalBuffer, 0);
        cheatConfig.MasterBreak = BitConverter.ToInt32(configGlobalBuffer, 4);
        cheatConfig.FaceitMode = (BitConverter.ToInt32(configGlobalBuffer, 8) != 0);
        cheatConfig.PanicKey = BitConverter.ToUInt32(configGlobalBuffer, 12);
        cheatConfig.BhopKey = BitConverter.ToUInt32(configGlobalBuffer, 16);
        cheatConfig.FriendlyFire = (BitConverter.ToInt32(configGlobalBuffer, 20) != 0);
        cheatConfig.PremiumUser_Visuals = (BitConverter.ToInt32(configGlobalBuffer, 24) != 0);
        cheatConfig.GlowESPKeybind = (cheatConfig.PremiumUser_Visuals) ? BitConverter.ToUInt32(configGlobalBuffer, 28) : 0u;
        cheatConfig.RadarEnabled = (BitConverter.ToInt32(configGlobalBuffer, 32) != 0);
        cheatConfig.NoFlashAmount = BitConverter.ToSingle(configGlobalBuffer, 36);
        cheatConfig.SoundESPEnabled = (BitConverter.ToInt32(configGlobalBuffer, 40) != 0);
        cheatConfig.SoundESPPitch = BitConverter.ToUInt32(configGlobalBuffer, 44);
        cheatConfig.SoundESPMaxRange = BitConverter.ToSingle(configGlobalBuffer, 48);
        int wepaonConfigIndex = 1;
        do
        {
            IntPtr weaponConfigPointer = AddToPointer(configData, 52 + wepaonConfigIndex * 76);
            if (wepaonConfigIndex >= 35)
            {
                if (wepaonConfigIndex > 34)
                {
                    byte[] specialWeaponBuffer = ReadInternalMemory(weaponConfigPointer, 12);
                    cheatConfig.TriggerKeybind[wepaonConfigIndex] = BitConverter.ToUInt32(specialWeaponBuffer, 0);
                    cheatConfig.TriggerDelay[wepaonConfigIndex] = BitConverter.ToInt32(specialWeaponBuffer, 4);
                    cheatConfig.TriggerPause[wepaonConfigIndex] = BitConverter.ToInt32(specialWeaponBuffer, 8);
                }
            }
            else
            {
                byte[] weaponConfigBuffer = ReadInternalMemory(weaponConfigPointer, 76);
                cheatConfig.AimbotKey[wepaonConfigIndex] = BitConverter.ToUInt32(weaponConfigBuffer, 0);
                cheatConfig.AimbotRotationAssist[wepaonConfigIndex] = (BitConverter.ToInt32(weaponConfigBuffer, 4) != 0);
                cheatConfig.AimbotFov[wepaonConfigIndex] = BitConverter.ToSingle(weaponConfigBuffer, 8);
                cheatConfig.AimbotSmooth[wepaonConfigIndex] = BitConverter.ToSingle(weaponConfigBuffer, 12);
                cheatConfig.AimbotBone[wepaonConfigIndex, 3] = BitConverter.ToInt32(weaponConfigBuffer, 16);
                cheatConfig.AimbotBone[wepaonConfigIndex, 2] = BitConverter.ToInt32(weaponConfigBuffer, 20);
                cheatConfig.AimbotBone[wepaonConfigIndex, 1] = BitConverter.ToInt32(weaponConfigBuffer, 24);
                cheatConfig.AimbotBone[wepaonConfigIndex, 0] = BitConverter.ToInt32(weaponConfigBuffer, 28);
                cheatConfig.AimbotThroughWalls[wepaonConfigIndex] = (BitConverter.ToInt32(weaponConfigBuffer, 32) != 0);
                cheatConfig.AimbotStick[wepaonConfigIndex] = BitConverter.ToInt32(weaponConfigBuffer, 36);
                cheatConfig.AimbotBoneSeletion[wepaonConfigIndex] = BitConverter.ToInt32(weaponConfigBuffer, 40);
                cheatConfig.AimDelay[wepaonConfigIndex] = BitConverter.ToInt32(weaponConfigBuffer, 44);
                cheatConfig.AimTime[wepaonConfigIndex] = BitConverter.ToInt32(weaponConfigBuffer, 48);
                cheatConfig.TriggerKeybind[wepaonConfigIndex] = BitConverter.ToUInt32(weaponConfigBuffer, 52);
                cheatConfig.TriggerDelay[wepaonConfigIndex] = BitConverter.ToInt32(weaponConfigBuffer, 56);
                cheatConfig.TriggerPause[wepaonConfigIndex] = BitConverter.ToInt32(weaponConfigBuffer, 60);
                cheatConfig.TriggerAfterShot[wepaonConfigIndex] = BitConverter.ToInt32(weaponConfigBuffer, 64);
                cheatConfig.VerticalRecoil[wepaonConfigIndex] = BitConverter.ToSingle(weaponConfigBuffer, 68);
                cheatConfig.HorizontalRecoil[wepaonConfigIndex] = BitConverter.ToSingle(weaponConfigBuffer, 72);
            }
            wepaonConfigIndex++;
        }
        while (wepaonConfigIndex <= 35);
        VirtualFree(pointerToPage, 0u, 32768u);
        WriteToOnlyReadableMemory(imageEndPointer, new byte[8]
        {
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        });
        try
        {
            if (loaderPID != 0)
            {
                Process.GetProcessById(loaderPID).Kill();
            }
        }
        catch (ArgumentException projectError)
        {
            ProjectData.SetProjectError(projectError);
            ProjectData.ClearProjectError();
        }
    }

    private static float TryParseFloatFromString(string str)
    {
        if (!float.TryParse(str, NumberStyles.Float, CultureInfo.InvariantCulture, out float result) || 1 == 0)
        {
            return 0f;
        }
        return result;
    }

    static bool FirstTime = true;
    private static string GetClipboardText()
    {
        //Default config, skip auth method
        if(FirstTime)
        {
            FirstTime = false;
            return "32 1 0 0 112 0 0 122 1 300 900 1 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 2 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 3 1 0 0 10.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 4 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 5 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 6 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 7 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 8 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 9 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 10 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 11 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 12 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 13 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 14 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 15 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 16 1 0 0 10.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 17 1 0 0 10.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 18 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 19 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 20 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 21 1 0 0 10.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 22 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 23 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 24 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 25 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 26 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 27 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 28 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 29 1 0 0 10.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 30 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 31 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 32 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 33 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 34 1 0 0 120.00 1 5 6 7 8 15.00 0 0 0 86 1 5 175 5.00 5.00 35 86 1 5 36 86 1 5";
        }

        if (!IsClipboardFormatAvailable(13u) || 1 == 0)
        {
            return null;
        }
        IntPtr intPtr = default;
        if (!OpenClipboard(intPtr) || 1 == 0)
        {
            return null;
        }
        string result = null;
        IntPtr intPtr2 = GetClipboardData(13u);
        if (intPtr2 != intPtr || 1 == 0)
        {
            IntPtr intPtr3 = GlobalLock(intPtr2);
            if (intPtr3 != IntPtr.Zero || 1 == 0)
            {
                result = Marshal.PtrToStringUni(intPtr3);
                GlobalUnlock(intPtr3);
            }
        }
        return result;
    }
}
